package tools;

import enums.failureClassification;
import enums.failureManualVerification;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.qas.qtest.api.services.execution.model.TestCycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pojo.*;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static enums.baseIdentifiers.projectAvonShop;

public class dataBuilder {
    Logger logger = LoggerFactory.getLogger(dataBuilder.class);

    /**
     *  This method is getting the full list of the test runs in an HttpEntity, and transforming it into a JSONArray.
     * @param inputEntity HttpEntity which contains JSON object(s).
     * @return Output will be a JSONArray which contains the list of JSONObjects under the "items" branch.
     * @throws IOException If something fails during the httpEntity->String transformation.
     * @throws org.json.simple.parser.ParseException If the JSON parser fails to parse the String.
     */
    public JSONArray getTestRunsFromResponse(HttpEntity inputEntity) throws IOException, org.json.simple.parser.ParseException {
        String responseStringRunResults = EntityUtils.toString(inputEntity);
        JSONObject fullResponseRunResults = (JSONObject) new JSONParser().parse(responseStringRunResults);
        return (JSONArray) fullResponseRunResults.get("items");
    }

    /**
     * This method is getting all the test runs under the given test-cycle.
     * As a page can contain only 1000 test runs, the method can paginate and collect all data.
     *
     * @param bearer     can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page
     * @param parentId   is the ID of the parent object which has the test runs.
     * @param parentType can be e.g.: "test-cycle"
     * @return the JSONArray of the test runs
     * @throws Exception Please check the stack trace for more hints.
     */
    public JSONArray getAllTestRunsFromParent(Long projectId, String bearer, String parentId, String parentType) throws Exception {
        httpRequestsGetAvonshop requestCreator = new httpRequestsGetAvonshop();
        Integer pageCounter = 1;
        Boolean needsNewRequest = true;
        JSONArray testRunList = new JSONArray();
        HttpEntity responseEntity = null;

        while (needsNewRequest) {
            responseEntity = requestCreator.getResponseEntityGet(requestCreator.setDefaultGETRequest(bearer, projectId + "/test-runs?parentId=" + parentId + "&parentType=" + parentType + "&expand=descendants&page=" + pageCounter.toString() + "&pageSize=1000"));
            String responseStringRunResults = EntityUtils.toString(responseEntity);
            JSONObject responseJsonObjectRunResults = (JSONObject) new JSONParser().parse(responseStringRunResults);
            JSONArray temporaryArray = (JSONArray)responseJsonObjectRunResults.get("items");
            testRunList.addAll(temporaryArray);
            Long total = (Long)responseJsonObjectRunResults.get("total");
            Long page = (Long)responseJsonObjectRunResults.get("page");
            Long pageSize = (Long)responseJsonObjectRunResults.get("page_size");
            if ((total-(page*pageSize)) > 0 ) {
                needsNewRequest = true;
                pageCounter++;
            } else { needsNewRequest=false;}
        }

        return testRunList;
    }

    /**
     * This method adding a group of basic data into the test run, like: ids, versions, name, etc
     * which is under the same parent element in the object.
     * @param selectedRun is  JSONObject which has the details for one test run
     * @param currentTestRun is a "test run" object for later usage
     */
    private void addBasicDataToSelectedRun(JSONObject selectedRun, testrun currentTestRun){
        currentTestRun.setTestCaseName((String)selectedRun.get("name"));
        currentTestRun.setTestRunName((String)selectedRun.get("pid"));
        currentTestRun.setTestCaseId((Long)selectedRun.get("testCaseId"));
        currentTestRun.setTestCaseVersion((String)selectedRun.get("test_case_version"));
        currentTestRun.setTestCaseVersionId((Long)selectedRun.get("test_case_version_id"));
        currentTestRun.setTestRunParentId((Long)selectedRun.get("parentId"));
        currentTestRun.setTestRunParentType((String)selectedRun.get("parentType"));
    }

    /**
     * This method is adding a group of property into the test run, like:status, env and market into the test run,
     * which is under the same parent element in the object.
     * @param currentTestRun is the "test run" object which we would like to update with Status (last run), Env, Market.
     * @param propertiesCurrentRun is the details of the given run
     */
    private void addBasicPropertiesToSelectedRun (testrun currentTestRun, JSONArray propertiesCurrentRun){
        for ( int subindexRuns = 0; subindexRuns < propertiesCurrentRun.size(); subindexRuns++ ) {
            JSONObject propertyOfRun = (JSONObject) propertiesCurrentRun.get(subindexRuns);
            if ( propertyOfRun.get("field_name").toString().equalsIgnoreCase("Status") ) {
                currentTestRun.setLastStatus(propertyOfRun.get("field_value_name").toString());
            }
            if (propertyOfRun.get("field_name").toString().equalsIgnoreCase("Env")) {
                currentTestRun.setEnv(propertyOfRun.get("field_value_name").toString());
            }
            if (propertyOfRun.get("field_name").toString().equalsIgnoreCase("Market")) {
                currentTestRun.setMarket(propertyOfRun.get("field_value_name").toString());
            }
        }
    }

    private boolean isStatusThis(JSONArray propertiesCurrentRun, String status) {
        switch (status) {
            case "descoped":
                for (int subindexRuns = 0; subindexRuns < propertiesCurrentRun.size(); subindexRuns++) {
                    JSONObject propertyOfRun = (JSONObject) propertiesCurrentRun.get(subindexRuns); //.get(0)
                    if (propertyOfRun.get("field_name").toString().equalsIgnoreCase("Status") && propertyOfRun.get("field_value_name").toString().equalsIgnoreCase("Descoped")) {
                        return true;
                    }
                }
                return false;
            case "failed":
                for (int subindexRuns = 0; subindexRuns < propertiesCurrentRun.size(); subindexRuns++) {
                    JSONObject propertyOfRun = (JSONObject) propertiesCurrentRun.get(subindexRuns); //.get(0)
                    if (propertyOfRun.get("field_name").toString().equalsIgnoreCase("Status") && propertyOfRun.get("field_value_name").toString().equalsIgnoreCase("Failed")) {
                        return true;
                    }
                }
                return false;
            default:
                return false;
        }


    }

    /**
     * This method is adding the executions with related information.
     * e.g.: status, test run start&end time, stack trace, classification, verification, comment.
     *
     * @param currentTestRun    is the "test run" object which we would like to update with last X run results
     * @param arrayOfRunResults is the JSONArray which contains the list of JSONObjects (the earlier test runs)
     */
    private void addLastRunResultsToSelectedRun(testrun currentTestRun, String testSet, JSONArray arrayOfRunResults) throws ParseException {
        ArrayList<execution> executionsTemporary = new ArrayList<execution>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        for ( int subindexResults = 0 ; subindexResults < arrayOfRunResults.size(); subindexResults++) {
            JSONObject resultSelectedRun = (JSONObject) arrayOfRunResults.get(subindexResults);
            JSONObject statusSelectedRun = (JSONObject) resultSelectedRun.get("status");
            String statusOfSelectedRun = statusSelectedRun.get("name").toString();
            String executionEndDateSelectedRun = (String) resultSelectedRun.get("exe_end_date");
            Date executionEndDate = dateFormat.parse(executionEndDateSelectedRun);
            String executionStartDateSelectedRun = (String) resultSelectedRun.get("exe_start_date");
            Date executionStartDate = dateFormat.parse(executionStartDateSelectedRun);
            execution currentExecution = new execution(statusOfSelectedRun, executionEndDate, executionStartDate);

            JSONArray propertiesOfRun = (JSONArray) resultSelectedRun.get("properties");
            JSONArray testStepsLog = (JSONArray) resultSelectedRun.get("test_step_logs");
            //SET automated run identifier, "buildIdentifier"
            for (int i = 0; i < propertiesOfRun.size(); i++) {
                JSONObject property = (JSONObject) propertiesOfRun.get(i);
                if (property.get("field_name").toString().equalsIgnoreCase("Automation build description")) {
                    currentExecution.setBuildIdentifier((String) property.get("field_value"));
                    break;
                }
            }
            //Check build identifier and if it not exists or it is too old, then stop the update of the execution
            //Too old means build id is older than requested amount of results + (amount/7)*2, considering the weekends too which is 2 days in every 7 days.
            LocalDateTime dateWithBuffer = createCurrentBuildId().minusDays((arrayOfRunResults.size() + ((arrayOfRunResults.size() / 7) * 2 * 2)));
            //LocalDateTime dateWithBuffer = setTimerForResults(testSet,arrayOfRunResults.size());
            if (currentExecution.getBuildIdentifier().isEmpty()) {
                break;
            } else if (
                    convertStringToIsoDateTime(currentExecution.getBuildIdentifier()).compareTo(convertStringToIsoDateTime((dateWithBuffer.toString()).substring(0, 19))) < 0) {
                break;
            }

            //create the execution if it has failed
            if (statusOfSelectedRun.equalsIgnoreCase("failed")) {

                //SET STACKTRACE FOR FAUILURE
                String traceFull = "";
                for (int i = 0; i < propertiesOfRun.size(); i++) {
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automated test stacktrace")) {
                        traceFull = (String) property.get("field_value");
                        break;
                    }
                }

                traceFull=traceFull.replaceAll("\n"," ").replaceAll("\r"," ");
                if (traceFull.length()>=255){
                    currentExecution.setCauseOfFailure(traceFull.substring(0,(255)));
                } else {
                    currentExecution.setCauseOfFailure(traceFull);
                }

                //SET CLASSIFICATION
                String classification = "";
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automation fail clasyfication")){
                        currentExecution.setClassificationOfFailure((String) property.get("field_value_name"));
                        break;
                    }
                }
                //SET VERIFICATION
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automation fail manual veryfication")){
                        currentExecution.setVerificationOfFailure((String) property.get("field_value_name"));
                        break;
                    }
                }

                //SET COMMENT
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automation fail comment")){
                        currentExecution.setCommentOfFailure((String) property.get("field_value"));
                        break;
                    }
                }

                //SET isAutoClassified
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automation fail autoclassified") &&
                            property.get("field_value_name").toString().equalsIgnoreCase("Yes")){
                        currentExecution.setAutoClassified(true);
                        break;
                    }
                }

                //SET STEP
                for (int i = 0; i < testStepsLog.size(); i++){
                    JSONObject step = (JSONObject) testStepsLog.get(i);
                    if (((JSONObject)step.get("status")).get("name").toString().equalsIgnoreCase("failed")){
                        currentExecution.setStepOfFailure(String.valueOf(step.get("order")));
                        break;
                    }
                }

                executionsTemporary.add(currentExecution);
            // create the execution if it has NOT failed
            } else {
                executionsTemporary.add(currentExecution);
            }
        }
        Collections.sort(executionsTemporary, (a, b) -> b.getExecutionEndDate().compareTo(a.getExecutionEndDate()));

        currentTestRun.setExecutions(executionsTemporary);
    }


    /**
     * This method is creating a testrun[] Array, listing all the input test runs with necessary data and properties coming from other methods.
     * If it is for only autoClassification, then collects only Failed test cases
     * Outcome will be used to map the data in the Env>>Market>>Feature structure.
     *
     * @param arrayOfTestRuns    is the full list of TestRuns (JSONObjects) in a JSONArray
     * @param bearer             can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page.
     * @param pageSizeRunResults is setting how many results you would like to see on 1 page (max is 999)
     * @return an array of test runs for later usages
     */
    public testrun[] createTestRuns(Long projectId, JSONArray arrayOfTestRuns, String bearer, Integer pageSizeRunResults, Boolean onlyClassification, String testSet) throws Exception {
        httpRequestsGetAvonshop requestHandler = new httpRequestsGetAvonshop();
        ArrayList<testrun> testruns = new ArrayList<>();
        for (int index = 0; index < arrayOfTestRuns.size(); index++) {
            /*  CREATE THE TEST-RUN  */
            JSONObject selectedRun = (JSONObject) arrayOfTestRuns.get(index);
            //Check if the latest result of the test run is Descoped, if yes, then skip it.
            //Check if now we are doing only autoClassification
            if (isStatusThis((JSONArray) selectedRun.get("properties"), "descoped") ||
                    (onlyClassification && !isStatusThis((JSONArray) selectedRun.get("properties"), "failed"))
            ) {
                continue;
                //if it is for report/summary and status is Descoped and below criteria is not fits, then jump to next iteration, else continue the current iteration.
                //if it is only for autoClassification and status is not Failed, then jump to next iteration, else continue the current iteration.
            } else {
                testrun currentTestRun = new testrun((Long) selectedRun.get("id"));
                /*  ADD DATA INTO THE TEST-RUN  */
                addBasicDataToSelectedRun(selectedRun, currentTestRun);
                addBasicPropertiesToSelectedRun(currentTestRun, (JSONArray) selectedRun.get("properties"));
                /*  ADD THE LAST RUNS INTO THE TEST-RUN  */
                addLastRunResultsToSelectedRun(
                        currentTestRun,testSet,
                        getTestRunsFromResponse(requestHandler.getTestRunLastResults(projectId, currentTestRun.getTestRunId().toString(), pageSizeRunResults.toString(), bearer))
                );
                /* If execution is not exists, because in previous method we skipped it due to missing buildId, the skip the current run
                 * because only executions with build id will be considered in the report.*/
                if (currentTestRun.getExecutions().isEmpty()) {
                    continue;
                }

                JSONArray linksSelectedRun = (JSONArray) selectedRun.get("links");
                JSONObject selfUrlApi = (JSONObject) linksSelectedRun.get(2);
                currentTestRun.setUrlApi(selfUrlApi.get("href").toString());
                currentTestRun.setUrlWebpage("https://avon.qtestnet.com/p/" + projectId.toString() + "/portal/project#tab=testexecution&object=3&id=" + currentTestRun.getTestRunId().toString());
                /*  ADD THE CURRENT TEST-RUN INTO THE LIST  */
                testruns.add(currentTestRun);
            }
        }

        //Create final array of the testruns
        testrun[] testrunsFinal = new testrun[testruns.size()];
        for (int counterTestRuns = 0; counterTestRuns < testruns.size(); counterTestRuns++) {
            testrunsFinal[counterTestRuns] = testruns.get(counterTestRuns);
        }


        return testrunsFinal;
    }


    /**
     * This method is creating the environment object with the structure of Environment > Market > Features
     *
     * @param testCycleId is one of the test cycles of PROD, QAM or QAF Automation root test cycles.
     * @return the created environment with Markets (Features included)
     */
    public environment createTestEnvironment(Long projectId, Long testCycleId) throws IOException {
        httpRequestsGetAvonshop requestCreator = new httpRequestsGetAvonshop();
        /*  GETTING THE TEST CYCLES */
        TestCycle testCycles = requestCreator.getAllTestCyclesInTestCycle(projectId, testCycleId);
        /*  CREATE THE ENVIRONMENT  */
        environment testEnvironment = new environment(testCycles.getName(), testCycles.getId(), projectId);
        /*  ADD THE MARKETS WITH FEATURES INTO THE ENVIRONMENT  */
        testEnvironment.setMarkets(
                provideMarketsIntoEnvironment(testCycles)
        );

        return testEnvironment;
    }

    /**
     * This method is putting all the "test run"s under the correct place in the Environment > Market > Feature structure,
     * and updating some useful counters, like result or classification amount, run times.
     * @param testruns is the Array of the "test run"s got from qTest
     * @param testEnvironment is a previously create environment object
     */
    public void buildUpTestEnvironment(testrun[] testruns, environment testEnvironment){
        for (int indexRunAmount = 0; indexRunAmount < testruns.length; indexRunAmount++) {
            for (int indexMarketAmount = 0; indexMarketAmount < testEnvironment.getMarkets().length; indexMarketAmount++) {
                for (int indexFeatureAmount = 0; indexFeatureAmount < testEnvironment.getMarkets()[indexMarketAmount].getFeatures().length; indexFeatureAmount ++){
                    if (testruns[indexRunAmount].getTestRunParentId().longValue() == testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestCycleId().longValue()) {
                        testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestruns().add(testruns[indexRunAmount]);
                        testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestruns()
                                .get(testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestruns().size()-1)
                                .setFeature(testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getName().
                                        substring(0,testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getName().length()-8));
                    }
                }
            }
        }

        setTestRunAmounts(testEnvironment);
        setEnvironmentCounters(testEnvironment);
    }

    /**
     * This method is creating the market object with the structure of Market > Features
     * @param testCycles is one of the test cycles of PROD, QAM or QAF Automation root test cycles
     * @return the Markets (Features included)
     */
    private market[] provideMarketsIntoEnvironment(TestCycle testCycles){
        /*  CREATE THE LIST OF MARKETS WITH FEATURES  */
        market[] testMarkets = new market[testCycles.getTestCycles().size()];
        //ENHANCED FOR LOOP is not applicable for TestCycles
        for (int index = 0; index < testCycles.getTestCycles().size(); index++) {
            testMarkets[index] = new market(testCycles.getTestCycles().get(index).getName(), testCycles.getTestCycles().get(index).getId());
            feature[] testFeatures = new feature[testCycles.getTestCycles().get(index).getTestCycles().get(0).getTestCycles().size()];
            for (int subindex = 0; subindex < testFeatures.length; subindex++) {
                testFeatures[subindex] = new feature(
                        testCycles.getTestCycles().get(index).getTestCycles().get(0).getTestCycles().get(subindex).getName(),
                        testCycles.getTestCycles().get(index).getTestCycles().get(0).getTestCycles().get(subindex).getId()
                );
            }
            testMarkets[index].setFeatures(testFeatures);
        }

        return testMarkets;
    }

    /**
     * This method organizes the test runs into "Passed Again"(0), "Failed Again"(1), "NEW Failure"(2), "NEW Pass"(3) and Other(4) groups.
     * @param testEnvironment  is the test environment which you are working with.
     * @return the ArrayList of the the mentioned test runs' groups.
     */
    public ArrayList<ArrayList<testrun>> getLastTwoResultsForComparison (environment testEnvironment) {
        ArrayList<ArrayList<testrun>> summary = new ArrayList<>();
        ArrayList<testrun> passAgain = new ArrayList<>();
        ArrayList<testrun> failAgain = new ArrayList<>();
        ArrayList<testrun> failNew = new ArrayList<>();
        ArrayList<testrun> passNew = new ArrayList<>();
        ArrayList<testrun> other = new ArrayList<>();

        for ( int indexMarket=0; indexMarket < testEnvironment.getMarkets().length; indexMarket++){
            for ( int indexFeature=0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++){
                for (int indexTestRun=0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++){
                    //CHECK if the test run has only 1 execution, and add it based on that into NEW Passed or Failed list.
                    if (testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().size() == 1){
                        String onlyResult = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).getResult();
                        if (onlyResult.equalsIgnoreCase("passed")){
                            passNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        if (onlyResult.equalsIgnoreCase("failed")){
                            failNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        other.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                    }
                    //If the test run has more than 1 results, then we do the comparison between the last 2 test run results and update the related lists based on the comparison.
                    else {
                        String currentResult = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).getResult();
                        String earlierResult = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(1).getResult();
                        /* Checking if Passed Again */
                        if (currentResult.equalsIgnoreCase(earlierResult) && currentResult.equalsIgnoreCase("passed")) {
                            passAgain.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Checking if Failed Again */
                        if (currentResult.equalsIgnoreCase(earlierResult) && currentResult.equalsIgnoreCase("failed")) {
                            failAgain.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Checking if new FAILURE */
                        if (currentResult.equalsIgnoreCase("failed") && earlierResult.equalsIgnoreCase("passed")) {
                            failNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Checking if new PASS */
                        if (currentResult.equalsIgnoreCase("passed") && earlierResult.equalsIgnoreCase("failed")) {
                            passNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Collecting Others */  //--> e.g. only 1 existing run execution, blocked, n/a, etc...
                        other.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                    }
                }

            }
        }
        summary.add(passAgain);
        summary.add(failAgain);
        summary.add(failNew);
        summary.add(passNew);
        summary.add(other);
        return summary;
    }

    /**
     *  This method sets the included test run amount for Features, Markets and the Environment.
     * @param testEnvironment is the test environment which you are working with.
     */
    private void setTestRunAmounts (environment testEnvironment)  {
        Integer amountRunEnvironment = 0;
        for (int indexMarketAmount = 0; indexMarketAmount < testEnvironment.getMarkets().length; indexMarketAmount++) {
            Integer amountRunMarket = 0;
            for (int indexFeatureAmount = 0; indexFeatureAmount < testEnvironment.getMarkets()[indexMarketAmount].getFeatures().length; indexFeatureAmount ++){
                testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].setTestRunAmount();
                amountRunMarket+=testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestRunAmount();
            }
            testEnvironment.getMarkets()[indexMarketAmount].setTestRunAmount(amountRunMarket);
            amountRunEnvironment+=testEnvironment.getMarkets()[indexMarketAmount].getTestRunAmount();
        }
        testEnvironment.setTestRunAmountLatest(amountRunEnvironment);
    }

    public void setEnvironmentCounters(environment testEnvironment){
        for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++) {
            for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++) {
                for (int indexTestRun = 0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++) {
                    testrun currentTestRun = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun);

                    //make sure that all ArrayLists have proper size
                    while (testEnvironment.getFailed().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getFailed().add(0);
                    }
                    while (testEnvironment.getPassed().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getPassed().add(0);
                    }
                    while (testEnvironment.getOther().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getOther().add(0);
                    }
                    while (testEnvironment.getRunTime().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getRunTime().add(0);
                    }
                    while (testEnvironment.getClassificationApplication().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationApplication().add(0);
                    }
                    while (testEnvironment.getClassificationTestScript().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationTestScript().add(0);
                    }
                    while (testEnvironment.getClassificationContent().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationContent().add(0);
                    }
                    while (testEnvironment.getClassificationAccess().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationAccess().add(0);
                    }
                    while (testEnvironment.getClassificationData().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationData().add(0);
                    }
                    while (testEnvironment.getClassificationConfiguration().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationConfiguration().add(0);
                    }
                    while (testEnvironment.getClassificationApplicationEnvironment().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationApplicationEnvironment().add(0);
                    }
                    while (testEnvironment.getClassificationTestautomationEnvironment().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationTestautomationEnvironment().add(0);
                    }
                    while (testEnvironment.getClassificationThirdPartyScriptIssue().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationThirdPartyScriptIssue().add(0);
                    }
                    while (testEnvironment.getClassificationPassedManually().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationPassedManually().add(0);
                    }
                    while (testEnvironment.getClassificationNotClassified().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getClassificationNotClassified().add(0);
                    }
                    while (testEnvironment.getAutoClassifiedAmount().size() < currentTestRun.getExecutions().size()){
                        testEnvironment.getAutoClassifiedAmount().add(0);
                    }

                    for (int indexExecution = 0; indexExecution < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().size(); indexExecution++) {

                        execution currentExecution = currentTestRun.getExecutions().get(indexExecution);

                        
                        if (currentExecution.getResult().equalsIgnoreCase("failed")){
                            testEnvironment.getFailed().set(indexExecution,testEnvironment.getFailed().get(indexExecution)+1);
                            String classification = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure();
                            switch (classification){
                                case "Application Defect":
                                    testEnvironment.getClassificationApplication().set(indexExecution,testEnvironment.getClassificationApplication().get(indexExecution)+1);
                                    break;
                                case "Test Script issue":
                                    testEnvironment.getClassificationTestScript().set(indexExecution,testEnvironment.getClassificationTestScript().get(indexExecution)+1);
                                    break;
                                case "Content issue":
                                    testEnvironment.getClassificationContent().set(indexExecution,testEnvironment.getClassificationContent().get(indexExecution)+1);
                                    break;
                                case "Access issue":
                                    testEnvironment.getClassificationAccess().set(indexExecution,testEnvironment.getClassificationAccess().get(indexExecution)+1);
                                    break;
                                case "Data issue":
                                    testEnvironment.getClassificationData().set(indexExecution,testEnvironment.getClassificationData().get(indexExecution)+1);
                                    break;
                                case "Configuration issues":
                                    testEnvironment.getClassificationConfiguration().set(indexExecution,testEnvironment.getClassificationConfiguration().get(indexExecution)+1);
                                    break;
                                case "Avon Shop Environment issue":
                                    testEnvironment.getClassificationApplicationEnvironment().set(indexExecution,testEnvironment.getClassificationApplicationEnvironment().get(indexExecution)+1);
                                    break;
                                case "Test Automation Environment issue":
                                    testEnvironment.getClassificationTestautomationEnvironment().set(indexExecution,testEnvironment.getClassificationTestautomationEnvironment().get(indexExecution)+1);
                                    break;
                                case "3rd Party script issue":
                                    testEnvironment.getClassificationThirdPartyScriptIssue().set(indexExecution,testEnvironment.getClassificationThirdPartyScriptIssue().get(indexExecution)+1);
                                    break;
                                default:
                                    if (testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getVerificationOfFailure().equalsIgnoreCase("pass manually")){
                                        testEnvironment.getClassificationPassedManually().set(indexExecution,testEnvironment.getClassificationPassedManually().get(indexExecution)+1);
                                    }
                                    else {
                                        testEnvironment.getClassificationNotClassified().set(indexExecution,testEnvironment.getClassificationNotClassified().get(indexExecution)+1);
                                    }
                                    break;
                            }
                            if (currentExecution.getAutoClassified()){
                                testEnvironment.getAutoClassifiedAmount().set(indexExecution,testEnvironment.getAutoClassifiedAmount().get(indexExecution)+1);
                            }
                        }
                        else if (currentExecution.getResult().equalsIgnoreCase("passed")){
                            testEnvironment.getPassed().set(indexExecution,testEnvironment.getPassed().get(indexExecution)+1);
                        }
                        else {
                            testEnvironment.getOther().set(indexExecution,testEnvironment.getOther().get(indexExecution)+1);
                        }

                        //update run times
                        Integer startTime = (int) (long) currentExecution.getExecutionStartDate().getTime();
                        Integer endTime = (int) (long) currentExecution.getExecutionEndDate().getTime();
                        testEnvironment.getRunTime().set(indexExecution, (testEnvironment.getRunTime().get(indexExecution)+(endTime-startTime)));

                    }
                }
            }
        }
        //set run times to minutes from milliseconds
        for ( int indexRunTime = 0; indexRunTime < testEnvironment.getRunTime().size(); indexRunTime++){
            testEnvironment.getRunTime().set(indexRunTime, (testEnvironment.getRunTime().get(indexRunTime)/60000));
        }
    }

    /**
     * This method helps to translate the classification into the ID which is used in qTest.
     * @param failureClassificationString is the name of the classification.
     * @return the id of the given classification
     */
    public String getFailureClassificationId (String failureClassificationString) {
        String classification;
        switch (failureClassificationString) {
            case "Application Defect":
                classification = failureClassification.applicationDefect.label;
                break;
            case "Test Script issue":
                classification = failureClassification.testScriptIssue.label;
                break;
            case "Content issue":
                classification = failureClassification.contentIssue.label;
                break;
            case "Access issue":
                classification = failureClassification.accessIssue.label;
                break;
            case "Data issue":
                classification = failureClassification.dataIssue.label;
                break;
            case "Configuration issue":
                classification = failureClassification.configurationIssue.label;
                break;
            case "Avon Shop Environment issue":
                classification = failureClassification.environmentApplicationIssue.label;
                break;
            case "Test Automation Environment issue":
                classification = failureClassification.environmentAutomationIssue.label;
                break;
            case "3rd Party script issue":
                classification = failureClassification.thirdPartyScriptIssue.label;
                break;
            default:
                classification = failureClassification.empty.label; //Empty
        }
        return classification;
    }

    /**
     * This method helps to translate the classification into the ID which is used in qTest.
     * @param failureVerificationString is the name of the verification.
     * @return the id of the given verification.
     */
    public String getFailureVerificationId (String failureVerificationString) {
        String verification;
        switch (failureVerificationString) {
            case "Not verified manually":
                verification = failureManualVerification.notVerified.label;
                break;
            case "Pass manually":
                verification = failureManualVerification.passManually.label;
                break;
            case "Fail manually":
                verification = failureManualVerification.failManually.label;
                break;
            default:
                verification = failureManualVerification.empty.label;
                break;
        }
        return verification;
    }

    /**
     * This method adds all element from arrayToCheck into arrayChangeList based on given criteria.
     * Besides it saves the id of the execution which has the same failure symptoms.
     * @param arrayToCheck original source of data
     * @param arrayChangeList source of the selected data
     */
    public void updateChangeList (ArrayList<testrun> arrayToCheck, ArrayList<testrun> arrayChangeList){
        //DO THE COMPARISON BETWEEN Failures - arrayToCheck testruns
        for (int runCounter = 0; runCounter < arrayToCheck.size(); runCounter++) {
            for (int executionRunCounter = 1; executionRunCounter < arrayToCheck.get(runCounter).getExecutions().size(); executionRunCounter++) {
                execution latestExecution = arrayToCheck.get(runCounter).getExecutions().get(0);
                execution previousExecution = arrayToCheck.get(runCounter).getExecutions().get(executionRunCounter);
                if (
                        //latest does not have classification
                        //previous has classification
                        //latest and previous "Cause of Failure" (stack trace) is same
                        //latest and previous "Step of Failure" (step where it failed) is same
                        latestExecution.getClassificationOfFailure().isEmpty() && !previousExecution.getClassificationOfFailure().isEmpty() &&
                                latestExecution.getCauseOfFailure().equalsIgnoreCase(previousExecution.getCauseOfFailure()) &&
                                latestExecution.getStepOfFailure().equalsIgnoreCase(previousExecution.getStepOfFailure())
                ) {
                    arrayToCheck.get(runCounter).setSimilarExecutionFailureId(executionRunCounter);
                    arrayChangeList.add(arrayToCheck.get(runCounter));
                    break;
                }
            }
        }
    }

    /**
     * This method updates the latest test log for all of the test runs coming from arrayChangeList,
     * with data from "similarExecution"-
     *
     * @param projectId       is related to the project in qTest which you are interested in.
     * @param arrayChangeList list of all the test run proposed for TestLog update.
     * @param testEnvironment is the test environment which you are working with.
     */
    public void updateTestLog(Long projectId, ArrayList<testrun> arrayChangeList, environment testEnvironment, Boolean isOnlyClassificaion) throws Exception {
        httpRequestModifyAvonShop poster = new httpRequestModifyAvonShop();
        httpRequestsGetAvonshop getter = new httpRequestsGetAvonshop();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        for (int i = 0; i < arrayChangeList.size(); i++) {

            //PREPARE data
            Integer similarExecutionFailureId = arrayChangeList.get(i).getSimilarFailureId();
            String startDate = dateFormat.format(arrayChangeList.get(i).getExecutions().get(0).getExecutionStartDate()) + "+00:00";
            String endDate = dateFormat.format(arrayChangeList.get(i).getExecutions().get(0).getExecutionEndDate()) + "+00:00";
            Long testRunId = arrayChangeList.get(i).getTestRunId();
            Long testLogIdLatest = getter.getLatestTestLogId(projectId, arrayChangeList.get(i).getTestRunId());
            String classificationId = getFailureClassificationId(arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getClassificationOfFailure());
            String verificationId = getFailureVerificationId(arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getVerificationOfFailure());
            String comment;

            if (arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getCommentOfFailure().contains("automated") ||
                    arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getCommentOfFailure().contains("Automated")) {
                comment = arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getCommentOfFailure();
            } else {
                comment = arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getCommentOfFailure()+"; modified by Automated Script";
            }
            while (comment.contains("\n")) {
                String newComment = comment.replaceAll("\n",";");
                comment=newComment;
            }

        //SEND update
            HttpResponse response = poster.updateLatestTestLog(
                    projectAvonShop.id,
                    testRunId,
                    testLogIdLatest,
                    startDate,
                    endDate,
                    classificationId,
                    comment,
                verificationId);
            //UPDATE data model if it is not only classification update and SEND has succeed.
            if (!isOnlyClassificaion && response.getStatusLine().getStatusCode() == 200) {
                Integer marketIndex = getMarketIndex(testEnvironment, arrayChangeList.get(i));
                Integer featureIndex = getFeatureIndex(testEnvironment, arrayChangeList.get(i), marketIndex);
                Integer testRunIndex = getTestRunIndex(testEnvironment, arrayChangeList.get(i), marketIndex, featureIndex);

                testEnvironment.getMarkets()[marketIndex].getFeatures()[featureIndex].getTestruns().get(testRunIndex).getExecutions().get(0).setCommentOfFailure(comment);
                testEnvironment.getMarkets()[marketIndex].getFeatures()[featureIndex].getTestruns().get(testRunIndex).getExecutions().get(0).setVerificationOfFailure(arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getVerificationOfFailure());
                testEnvironment.getMarkets()[marketIndex].getFeatures()[featureIndex].getTestruns().get(testRunIndex).getExecutions().get(0).setClassificationOfFailure(arrayChangeList.get(i).getExecutions().get(similarExecutionFailureId).getClassificationOfFailure());
                testEnvironment.getMarkets()[marketIndex].getFeatures()[featureIndex].getTestruns().get(testRunIndex).getExecutions().get(0).setAutoClassified(true);
            }
        }
    }

    /**
     * This method finds the market of a given test execution.
     * @param testEnvironment is the test environment which you are working with.
     * @param testRun which contains the test execution
     * @return the id of the market, if missing then -1
     */
    private Integer getMarketIndex (environment testEnvironment, testrun testRun){
        for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++) {
            if (testEnvironment.getMarkets()[indexMarket].getName().equalsIgnoreCase(testRun.getMarket())){
                return indexMarket;
            }
        }
        return -1;
    }

    /**
     * This method finds the feature of a given test execution.
     * @param testEnvironment is the test environment which you are working with.
     * @param testRun which contains the test execution
     * @param marketIndex is the market where you can find the test execution
     * @return the id of the feature, if missing then -1
     */
    private Integer getFeatureIndex (environment testEnvironment, testrun testRun, Integer marketIndex){
        for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[marketIndex].getFeatures().length; indexFeature++) {
            if (testEnvironment.getMarkets()[marketIndex].getFeatures()[indexFeature].getName().contains(testRun.getFeature())){
                return indexFeature;
            }
        }
        return -1;
    }

    /**
     * This method finds the feature of a given test execution.
     * @param testEnvironment is the test environment which you are working with.
     * @param testRun is what you are looking for in the data model
     * @param marketIndex is the market where you can find the test execution
     * @param featureIndex is the feature within the market where you can find the test execution
     * @return the id of the test run
     */
    private Integer getTestRunIndex(environment testEnvironment, testrun testRun, Integer marketIndex, Integer featureIndex) {
        for (int indexTestRun = 0; indexTestRun < testEnvironment.getMarkets()[marketIndex].getFeatures()[featureIndex].getTestruns().size(); indexTestRun++) {
            if (testEnvironment.getMarkets()[marketIndex].getFeatures()[featureIndex].getTestruns().get(indexTestRun).getTestRunName().equalsIgnoreCase(testRun.getTestRunName())) {
                return indexTestRun;
            }
        }
        return -1;
    }

    private LocalDateTime createCurrentBuildId() throws ParseException {
        return LocalDateTime.now();
    }
    private LocalDateTime setTimerForResults ( String envName, Integer resultSize ){
        LocalDateTime timer = LocalDateTime.now();
        //define the date before you are not interested in the results. It will be used for excel graphs
        switch (envName) {
            case "QAM_ALL":
                Integer weeks = 10;
                if (resultSize < weeks) {timer.minusWeeks(resultSize);}
                else {timer.minusWeeks(weeks);}
                return timer;
            default:
                timer.minusDays((resultSize + (resultSize / 7) * 2 + 2));
                return timer;
        }

    }

    private LocalDateTime convertStringToIsoDateTime(String buildId) throws ParseException {
        return LocalDateTime.parse(buildId, DateTimeFormatter.ISO_DATE_TIME);
    }
}
