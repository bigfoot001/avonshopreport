package tools;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.qas.qtest.api.auth.PropertiesQTestCredentials;
import org.qas.qtest.api.auth.QTestCredentials;
import org.qas.qtest.api.services.execution.TestExecutionService;
import org.qas.qtest.api.services.execution.TestExecutionServiceClient;
import org.qas.qtest.api.services.execution.model.GetLastLogRequest;
import org.qas.qtest.api.services.execution.model.GetTestCycleRequest;
import org.qas.qtest.api.services.execution.model.TestCycle;
import org.qas.qtest.api.services.execution.model.TestLog;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class httpRequestsGetAvonshop {
    String urlQtestAvonshop = "https://avon.qtestnet.com/api/v3/projects/";
    //Long projectId = baseIdentifiers.projectAvonShop.id;

    /**
     * This method provides the id of the latest test log of the given test run.
     *
     * @param testRunId qTest Test Run ID
     * @return long value of the id.
     */
    public Long getLatestTestLogId(Long projectId, Long testRunId) throws IOException {

        QTestCredentials credentials = new PropertiesQTestCredentials(new ByteArrayInputStream(("token = Bearer " + System.getProperties().get("qtestBearer")).getBytes()));
        TestExecutionService testExecutionService = new TestExecutionServiceClient(credentials);
        testExecutionService.setEndpoint("avon.qtestnet.com");
        GetLastLogRequest getLastLogRequest = new GetLastLogRequest().withProjectId(projectId).withTestRunId(testRunId);
        TestLog testLog = testExecutionService.getLastLog(getLastLogRequest);

        return testLog.getId();
    }

    /**
     * This method gives back an HttpEntity which contains the last results of a specific test run.
     *
     * @param testRunId qTest Test Run ID
     * @param pageSize  shows how many results you would like to see on 1 page, max: 1000
     * @param bearer    can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page.
     * @return Outcome will be an HttpEntity for later usage.
     */
    public HttpEntity getTestRunLastResults(Long projectId, String testRunId, String pageSize, String bearer) throws Exception {
        return getResponseEntityGet(setDefaultGETRequest(bearer, projectId.toString() + "/test-runs/" + testRunId + "/test-logs?pageSize=" + pageSize + "&page=1"));
    }

    /**
     * This method gives back a Test Cycle with all the descendant test cycles. (only test cycles)
     *
     * @param testCycleId is one of the test cycles of PROD, QAM or QAF Automation root test cycles
     * @return a TestCycle object with all the included test cycles.
     */
    public TestCycle getAllTestCyclesInTestCycle(Long projectId, Long testCycleId) throws IOException {
        /*  SETUP THE REQUEST  */
        String endpoint = "avon.qtestnet.com";
        QTestCredentials credentials = new PropertiesQTestCredentials(new ByteArrayInputStream(("token = Bearer " + System.getProperties().get("qtestBearer")).getBytes()));
        TestExecutionService testExecutionService = new TestExecutionServiceClient(credentials);
        testExecutionService.setEndpoint(endpoint);
        GetTestCycleRequest getTestCycleRequestDescendants = new GetTestCycleRequest().withProjectId(projectId).withTestCycleId(testCycleId);
        getTestCycleRequestDescendants.setIncludeDescendants(true);
        /*  GETTING THE TEST CYCLES */
        TestCycle testCycles = null;
        testCycles = testExecutionService.getTestCycle(getTestCycleRequestDescendants);

        return testCycles;
    }

    /**
     * This method set up a basic GET request to qTest apis.
     * @param url endpoint where you would like to send your GET request
     * @param bearer can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page.
     * @return Outcome will be a basic HttpGet entity with url and headers.
     */
    HttpGet setDefaultGETRequest(String bearer, String url){
            HttpGet originalRequest = new HttpGet(urlQtestAvonshop + url);
            originalRequest.setHeader("accept", "application/json;charset=UTF-8");
            originalRequest.setHeader("accept-encoding", "gzip");
            originalRequest.setHeader("authorization", bearer);
            originalRequest.setHeader("content-type", "application/json;charset=UTF-8");
            originalRequest.setHeader("user-agent", "http-auth-client-java/1.4.2 Windows_10/10.0 OpenJDK_64-Bit_Server_VM/11.0.4+10-b520.11");
            return originalRequest;
    }

    /**
     *  This method is providing and HttpEntity based on the response to your GET request.
     *  This entity contains the JSONObjects and JSONArrays for further usages.
     * @param givenRequest It should be an "HttpGet" entity already with url and headers.
     * @return Outcome will be an HttpEntity, the response for the GET request.
     */
    HttpEntity getResponseEntityGet(HttpGet givenRequest) throws Exception {
        if (givenRequest.getMethod().equalsIgnoreCase("GET")) {

                CloseableHttpClient httpClientGet = HttpClientBuilder.create().build();
                HttpResponse response = httpClientGet.execute(givenRequest);
                HttpEntity entity = response.getEntity();
                return entity;
        }
        throw new Exception("You need to give a GET request (HttpGet) to this method. In any other cases it fails.");
    }
}
