package tools;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.XDDFColor;
import org.apache.poi.xddf.usermodel.XDDFLineProperties;
import org.apache.poi.xddf.usermodel.XDDFShapeProperties;
import org.apache.poi.xddf.usermodel.XDDFSolidFillProperties;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;
import pojo.environment;
import pojo.testrun;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class buildExcelWorkbook {
    private Workbook workBookQualityReport = new XSSFWorkbook();
    private tools.dataBuilder dataBuilder = new dataBuilder();
    private CreationHelper creationHelper = workBookQualityReport.getCreationHelper();

    /**
     * This method creates the workbook with it's sheets.
     * @param testEnvironment  the test environment which has the data model for the graph
     * @return provide the workbook for later usage
     */
    public Workbook createWorkbookQualityReport(environment testEnvironment) {

        //executive summary page
        createExecutiveReportSheet(testEnvironment);
        //cover page with quality index; test run results by % and classification trend graphs
        createQualityReportSheet(testEnvironment);
        //graphs: 1. better, worse, still fail, passed again

        ArrayList<ArrayList<testrun>> lastTwoRunsForAll = dataBuilder.getLastTwoResultsForComparison(testEnvironment);
        createCoverPageFailureAnalyses(lastTwoRunsForAll);
        if (lastTwoRunsForAll.get(2).size()>0) {
            createSheetsForFailureAnalyses(lastTwoRunsForAll.get(2), "NEW Failed");
        }
        if (lastTwoRunsForAll.get(1).size()>0) {
            createSheetsForFailureAnalyses(lastTwoRunsForAll.get(1), "Failed again");
        }
        if (lastTwoRunsForAll.get(4).size()>0){
            createSheetsForFailureAnalyses(lastTwoRunsForAll.get(4),"Other");
        }

        return workBookQualityReport;
    }

    /**
     * This method creates the executive summary report sheet which shows a brief summary about the current status.
     * @param testEnvironment the test environment which has the data model for the graph
     */
    private void createExecutiveReportSheet(environment testEnvironment) {
        XSSFSheet executiveReportSheet = (XSSFSheet) workBookQualityReport.createSheet("Executive Summary");

        Integer rowAmount = 50;
        Integer columnAmount = 15;
        createCells(executiveReportSheet,rowAmount,columnAmount);

        CellStyle percentageStyle = executiveReportSheet.getWorkbook().createCellStyle();
        percentageStyle.setDataFormat(executiveReportSheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(9)));
        CellStyle averageStyle = executiveReportSheet.getWorkbook().createCellStyle();
        averageStyle.setDataFormat(executiveReportSheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(1)));
        averageStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        averageStyle.setAlignment(HorizontalAlignment.CENTER);

        Integer currentPassedCorrected = testEnvironment.getPassed().get(0)+testEnvironment.getClassificationPassedManually().get(0);
        Integer previousPassedCorrected = testEnvironment.getPassed().get(1)+testEnvironment.getClassificationPassedManually().get(1);
        Integer currentFailedCorrected = testEnvironment.getFailed().get(0)-testEnvironment.getClassificationPassedManually().get(0);
        Integer previousFailedCorrected = testEnvironment.getFailed().get(1)-testEnvironment.getClassificationPassedManually().get(1);
        Integer currentSumExecutions = testEnvironment.getFailed().get(0)+testEnvironment.getPassed().get(0);//+testEnvironment.getOther().get(0);
        Integer previousSumExecutions = testEnvironment.getFailed().get(1)+testEnvironment.getPassed().get(1);//+testEnvironment.getOther().get(1);

        //Summarize QualityIndex changes
//        Float currentQualityIndex = (((float)(currentPassedCorrected-currentFailedCorrected))/(currentSumExecutions));
//        Float previousQualityIndex = (((float)(previousPassedCorrected-previousFailedCorrected))/(previousSumExecutions));
        Float currentQualityIndex = ((currentPassedCorrected/(float)currentSumExecutions))-((currentFailedCorrected/(float)currentSumExecutions));
        Float previousQualityIndex = ((previousPassedCorrected/(float)previousSumExecutions))-((previousFailedCorrected/(float)previousSumExecutions));
        executiveReportSheet.getRow(0).getCell(0).setCellValue("EXECUTIVE SUMMARY");
        executiveReportSheet.getRow(1).getCell(0).setCellValue("Latest Quality Index is:");
        executiveReportSheet.getRow(1).getCell(1).setCellStyle(percentageStyle);
        executiveReportSheet.getRow(1).getCell(1).setCellValue(currentQualityIndex);
        executiveReportSheet.getRow(2).getCell(0).setCellValue("Quality Index has");
        if (currentQualityIndex>previousQualityIndex){
            executiveReportSheet.getRow(2).getCell(0).setCellValue("Quality Index has INCREASED by");
            executiveReportSheet.getRow(2).getCell(1).setCellStyle(percentageStyle);
            executiveReportSheet.getRow(2).getCell(1).setCellValue(currentQualityIndex-previousQualityIndex);
        }
        else if (previousQualityIndex>currentQualityIndex){
            executiveReportSheet.getRow(2).getCell(0).setCellValue("Quality Index has DECREASED by");
            executiveReportSheet.getRow(2).getCell(1).setCellStyle(percentageStyle);
            executiveReportSheet.getRow(2).getCell(1).setCellValue(previousQualityIndex-currentQualityIndex);
        }
        else {
            executiveReportSheet.getRow(2).getCell(0).setCellValue("Quality Index has NOT CHANGED");
        }

        //Summarize failure changes
        executiveReportSheet.getRow(3).getCell(0).setCellValue("Total FAILED test executions");
        executiveReportSheet.getRow(3).getCell(1).setCellStyle(averageStyle);
        executiveReportSheet.getRow(3).getCell(1).setCellValue(currentFailedCorrected);
        executiveReportSheet.getRow(4).getCell(0).setCellValue("Failure amount has");
        if (currentFailedCorrected>previousFailedCorrected){
            executiveReportSheet.getRow(4).getCell(0).setCellValue("Failure amount has INCREASED by");
            executiveReportSheet.getRow(4).getCell(1).setCellStyle(averageStyle);
            executiveReportSheet.getRow(4).getCell(1).setCellValue(currentFailedCorrected-previousFailedCorrected);
        }
        if (previousFailedCorrected>currentFailedCorrected){
            executiveReportSheet.getRow(4).getCell(0).setCellValue("Failure amount has DECREASED by");
            executiveReportSheet.getRow(4).getCell(1).setCellStyle(averageStyle);
            executiveReportSheet.getRow(4).getCell(1).setCellValue(previousFailedCorrected-currentFailedCorrected);
        }
        if (currentFailedCorrected==previousFailedCorrected){
            executiveReportSheet.getRow(4).getCell(0).setCellValue("Failure amount has NOT CHANGED");
        }

        //Summarize failures by classification
        executiveReportSheet.getRow(6).getCell(0).setCellValue("Out of "+currentFailedCorrected+" Failed test scenario we have:");
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,7,"Application Defect",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,8,"Test Script issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,9,"Content issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,10,"Access issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,11,"Data issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,12,"Configuration issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,13,"Avon Shop Environment issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,14,"Test Automation Environment issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,15,"3rd Party script issue",averageStyle);
        setClassificationsOnExecutiveSummarySheet(testEnvironment,executiveReportSheet,17,"PASSED during manual check",averageStyle);


        Integer notClassified = currentFailedCorrected -
                (testEnvironment.getClassificationApplication().get(0) +
                        testEnvironment.getClassificationTestScript().get(0) +
                        testEnvironment.getClassificationContent().get(0) +
                        testEnvironment.getClassificationAccess().get(0) +
                        testEnvironment.getClassificationData().get(0) +
                        testEnvironment.getClassificationConfiguration().get(0) +
                        testEnvironment.getClassificationApplicationEnvironment().get(0) +
                        testEnvironment.getClassificationTestautomationEnvironment().get(0) +
                        testEnvironment.getClassificationThirdPartyScriptIssue().get(0));
        executiveReportSheet.getRow(18).getCell(0).setCellValue("NOT CLASSIFIED");
        executiveReportSheet.getRow(18).getCell(1).setCellStyle(averageStyle);
        executiveReportSheet.getRow(18).getCell(1).setCellValue(notClassified);

        for (int indexColumn = 0; indexColumn < columnAmount; indexColumn++ ){
            executiveReportSheet.autoSizeColumn(indexColumn);
        }

    }

    /**
     * This method creates the quality report sheet which shows the trends of different activities in test automation.
     * @param testEnvironment  the test environment which has the data model for the graph
     */
    public void createQualityReportSheet(environment testEnvironment) {
        XSSFSheet qualityIndexSheet = (XSSFSheet) workBookQualityReport.createSheet("Quality Index");

        createCells(qualityIndexSheet,200,40);

        Integer dataAmount = setDataQualityIndexSheet(qualityIndexSheet,testEnvironment);
        graphQualityIndexTrend(qualityIndexSheet,"Quality Index",1,1,(int)Math.ceil(dataAmount*0.6),1,7, 8, dataAmount);
        graphTestExecutionPercentage(qualityIndexSheet,"Test Run Results by % - corrected",1,(1+15),(int)Math.ceil(dataAmount*0.6),1,6,23,22, dataAmount);
        graphTestExecutionPercentage(qualityIndexSheet,"Test Run Results by %",1,(1+15+15),(int)Math.ceil(dataAmount*0.6),1,6,5,4, dataAmount);
        graphClassificationTrends(qualityIndexSheet,testEnvironment,"Failures by Classification",1,(1+15+15+15),(int)Math.ceil(dataAmount*0.6),1,
                dataAmount,10,11,12,13,14,15,
                16,17,18,9,19);
        graphSingleLine(qualityIndexSheet,"Test Run amount change",1,(1+15+15+15+25),(int)Math.ceil(dataAmount*0.6),1,3, dataAmount);
        graphSingleLine(qualityIndexSheet,"amount of auto-classified failures",1,(1+15+15+15+25+15),(int)Math.ceil(dataAmount*0.6),1,24, dataAmount);
        graphSingleLine(qualityIndexSheet,"Test execution time (hours)",1,(1+15+15+15+25+15+15),(int)Math.ceil(dataAmount*0.6),1,27, dataAmount);
        graphSingleLine(qualityIndexSheet,"Average test execution time per test run (minutes)",1,(1+15+15+15+25+15+15+15),(int)Math.ceil(dataAmount*0.6),1,26, dataAmount);
    }

    /**
     * Creates the cover page (summary) for the Failure analyses workbook
     * @param lastTwoRunsForAll list of all test runs grouped based on the comparison of the last 2 test run of each test case
     */
    private void createCoverPageFailureAnalyses (ArrayList<ArrayList<testrun>> lastTwoRunsForAll) {
        String[] columnsTitles = new String[]{"execution","amount"};
        String[] rowTitles = new String[]{"Passed again","NEW Passed","Failed again","NEW Failed","Other"};
        XSSFSheet sheetLastResultsSummary = (XSSFSheet) workBookQualityReport.createSheet("Last Result Summary");
        createCells(sheetLastResultsSummary,30,30);

        createHeader(sheetLastResultsSummary,columnsTitles);

        Font summaryFont = workBookQualityReport.createFont();
        summaryFont.setColor(IndexedColors.BLACK.getIndex());
        CellStyle summaryCells = workBookQualityReport.createCellStyle();
        summaryCells.setAlignment(HorizontalAlignment.CENTER);
        summaryCells.setVerticalAlignment(VerticalAlignment.CENTER);
        summaryCells.setFont(summaryFont);


        //Row of Passed again
        sheetLastResultsSummary.getRow(1).getCell(0).setCellValue(rowTitles[0]);
        sheetLastResultsSummary.getRow(1).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(1).getCell(1).setCellValue(lastTwoRunsForAll.get(0).size());
        sheetLastResultsSummary.getRow(1).getCell(1).setCellStyle(summaryCells);

        //Row of NEW Passed
        sheetLastResultsSummary.getRow(2).getCell(0).setCellValue(rowTitles[1]);
        sheetLastResultsSummary.getRow(2).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(2).getCell(1).setCellValue(lastTwoRunsForAll.get(3).size());
        sheetLastResultsSummary.getRow(2).getCell(1).setCellStyle(summaryCells);

        //Row of Failed again
        sheetLastResultsSummary.getRow(3).getCell(0).setCellValue(rowTitles[2]);
        sheetLastResultsSummary.getRow(3).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(3).getCell(1).setCellValue(lastTwoRunsForAll.get(1).size());
        sheetLastResultsSummary.getRow(3).getCell(1).setCellStyle(summaryCells);

        //Row of NEW Failed
        sheetLastResultsSummary.getRow(4).getCell(0).setCellValue(rowTitles[3]);
        sheetLastResultsSummary.getRow(4).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(4).getCell(1).setCellValue(lastTwoRunsForAll.get(2).size());
        sheetLastResultsSummary.getRow(4).getCell(1).setCellStyle(summaryCells);

        //Row of Other
        sheetLastResultsSummary.getRow(5).getCell(0).setCellValue(rowTitles[4]);
        sheetLastResultsSummary.getRow(5).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(5).getCell(1).setCellValue(lastTwoRunsForAll.get(4).size());
        sheetLastResultsSummary.getRow(5).getCell(1).setCellStyle(summaryCells);

        for ( int indexColumn = 0; indexColumn < 2; indexColumn++) {
            sheetLastResultsSummary.autoSizeColumn(indexColumn);
        }

    }

    /**
     * This method create an additional sheet which shows test runs with details like: market, feature, title, url.
     * @param testrunList is list of test runs which previously passed, but last time failed.
     */
    private void createSheetsForFailureAnalyses(ArrayList<testrun> testrunList, String sheetTitle ){
        //Create sheet
        XSSFSheet sheet = (XSSFSheet) workBookQualityReport.createSheet(sheetTitle);
        createCells(sheet,1,10);
        // SET the Styles
        //GENERIC CELL
        CellStyle genericCell = workBookQualityReport.createCellStyle();
        genericCell.setAlignment(HorizontalAlignment.LEFT);
        genericCell.setVerticalAlignment(VerticalAlignment.CENTER);
        //HIGHLIGHTED CELL
        Font highlightedFont = workBookQualityReport.createFont();
        highlightedFont.setBold(true);
        highlightedFont.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
        CellStyle highlightedCell = workBookQualityReport.createCellStyle();
        highlightedCell.setFont(highlightedFont);
        highlightedCell.setAlignment(HorizontalAlignment.LEFT);
        highlightedCell.setVerticalAlignment(VerticalAlignment.CENTER);
        //MANUALLY VERIFIED CELL
        Font manuallyVerifiedFont = workBookQualityReport.createFont();
        manuallyVerifiedFont.setBold(true);
        manuallyVerifiedFont.setColor(HSSFColor.HSSFColorPredefined.BLUE.getIndex());
        CellStyle manuallyVerifiedCell = workBookQualityReport.createCellStyle();
        manuallyVerifiedCell.setFont(manuallyVerifiedFont);
        manuallyVerifiedCell.setAlignment(HorizontalAlignment.RIGHT);
        manuallyVerifiedCell.setVerticalAlignment(VerticalAlignment.CENTER);
        //URL CELL
        Font urlFont = workBookQualityReport.createFont();
        urlFont.setColor(IndexedColors.BLUE.getIndex());
        CellStyle urlCell = workBookQualityReport.createCellStyle();
        urlCell.setFont(urlFont);
        urlCell.setAlignment(HorizontalAlignment.CENTER);
        urlCell.setVerticalAlignment(VerticalAlignment.CENTER);

        String[] columnsTitles;
        Integer rowPosition = 1;
        if ( !testrunList.isEmpty() && testrunList.get(0).getExecutions().get(0).getResult().equalsIgnoreCase("failed") &&
                testrunList.get(0).getExecutions().get(1).getResult().equalsIgnoreCase("failed")){
            columnsTitles = new String[]{"Market", "Feature", "Test Run ID", "Test Case title", "Current Exception", "Previous Exception", "Current Failed Step", "Previous Failed Step", "Classification"};
            createHeader(sheet, columnsTitles);
            // Create the Cells and fill them with data, with added style
            for ( int indexTestRun = 0; indexTestRun < testrunList.size(); indexTestRun++){
                Row temporaryRow = sheet.createRow(rowPosition);
                //Market
                temporaryRow.createCell(0).setCellValue(testrunList.get(indexTestRun).getMarket());
                temporaryRow.getCell(0).setCellStyle(genericCell);
                //Feature
                temporaryRow.createCell(1).setCellValue(testrunList.get(indexTestRun).getFeature());
                temporaryRow.getCell(1).setCellStyle(genericCell);
                XSSFHyperlink hyperlink = (XSSFHyperlink) creationHelper.createHyperlink(HyperlinkType.URL);
                hyperlink.setAddress(testrunList.get(indexTestRun).getUrlWebpage());
                //TestRun ID
                temporaryRow.createCell(2).setCellValue(testrunList.get(indexTestRun).getTestRunName());
                temporaryRow.getCell(2).setHyperlink(hyperlink);
                temporaryRow.getCell(2).setCellStyle(urlCell);
                //TestCaseTitle
                temporaryRow.createCell(3).setCellValue(testrunList.get(indexTestRun).getTestCaseName());
                temporaryRow.getCell(3).setCellStyle(genericCell);
                //Current Exception (4) and Previous Exception (5)
                temporaryRow.createCell(4).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getCauseOfFailure());
                if (testrunList.get(0).getExecutions().get(0).getCauseOfFailure().equalsIgnoreCase(testrunList.get(0).getExecutions().get(1).getCauseOfFailure())){
                    temporaryRow.createCell(5).setCellValue("SAME EXCEPTION");
                    temporaryRow.getCell(4).setCellStyle(genericCell);
                    temporaryRow.getCell(5).setCellStyle(genericCell);
                } else {
                    temporaryRow.createCell(5).setCellValue(testrunList.get(indexTestRun).getExecutions().get(1).getCauseOfFailure());
                    temporaryRow.getCell(4).setCellStyle(highlightedCell);
                    temporaryRow.getCell(5).setCellStyle(genericCell);
                }
                //Failed step
                temporaryRow.createCell(6).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getStepOfFailure());
                if (testrunList.get(indexTestRun).getExecutions().get(0).getStepOfFailure().equalsIgnoreCase(testrunList.get(indexTestRun).getExecutions().get(1).getStepOfFailure())) {
                    temporaryRow.createCell(7).setCellValue("SAME STEP");
                    temporaryRow.getCell(6).setCellStyle(genericCell);
                    temporaryRow.getCell(7).setCellStyle(genericCell);
                } else {
                    temporaryRow.createCell(7).setCellValue(testrunList.get(indexTestRun).getExecutions().get(1).getStepOfFailure());
                    temporaryRow.getCell(6).setCellStyle(highlightedCell);
                    temporaryRow.getCell(7).setCellStyle(genericCell);
                }
                //Classification
                if (testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure().isEmpty()) {
                    if (testrunList.get(indexTestRun).getExecutions().get(0).getVerificationOfFailure().isEmpty()) {
                        temporaryRow.createCell(8).setCellValue("MANUAL CHECK NECESSARY");
                        temporaryRow.getCell(8).setCellStyle(highlightedCell);
                    } else {
                        temporaryRow.createCell(8).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getVerificationOfFailure());
                        temporaryRow.getCell(8).setCellStyle(manuallyVerifiedCell);
                    }
                    sheet.autoSizeColumn(8);
                } else {
                    temporaryRow.createCell(8).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure());
                    temporaryRow.getCell(8).setCellStyle(genericCell);
                    sheet.autoSizeColumn(8);
                }
                rowPosition++;
            }
        } else if (testrunList.get(0).getExecutions().get(0).getResult().equalsIgnoreCase("failed") &&
            !testrunList.get(0).getExecutions().get(0).getResult().equalsIgnoreCase(testrunList.get(0).getExecutions().get(1).getResult())) {
            columnsTitles = new String[]{"Market","Feature","Test Run ID","Test Case title", "Current Exception", "Failed Step", "Classification"};
            createHeader(sheet,columnsTitles);
            // Create the Cells and fill them with data, with added style
            for ( int indexTestRun = 0; indexTestRun < testrunList.size(); indexTestRun++) {
                Row temporaryRow = sheet.createRow(rowPosition);
                temporaryRow.createCell(0).setCellValue(testrunList.get(indexTestRun).getMarket());
                temporaryRow.getCell(0).setCellStyle(genericCell);
                temporaryRow.createCell(1).setCellValue(testrunList.get(indexTestRun).getFeature());
                temporaryRow.getCell(1).setCellStyle(genericCell);
                XSSFHyperlink hyperlink = (XSSFHyperlink) creationHelper.createHyperlink(HyperlinkType.URL);
                hyperlink.setAddress(testrunList.get(indexTestRun).getUrlWebpage());
                temporaryRow.createCell(2).setCellValue(testrunList.get(indexTestRun).getTestRunName());
                temporaryRow.getCell(2).setHyperlink(hyperlink);
                temporaryRow.getCell(2).setCellStyle(urlCell);
                temporaryRow.createCell(3).setCellValue(testrunList.get(indexTestRun).getTestCaseName());
                temporaryRow.getCell(3).setCellStyle(genericCell);
                temporaryRow.createCell(4).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getCauseOfFailure());
                temporaryRow.getCell(4).setCellStyle(genericCell);
                temporaryRow.createCell(5).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getStepOfFailure());
                if (testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure().isEmpty()){
                    if (testrunList.get(indexTestRun).getExecutions().get(0).getVerificationOfFailure().isEmpty()){
                        temporaryRow.createCell(6).setCellValue("MANUAL CHECK NECESSARY");
                        temporaryRow.getCell(6).setCellStyle(highlightedCell);
                    } else {
                        temporaryRow.createCell(6).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getVerificationOfFailure());
                        temporaryRow.getCell(6).setCellStyle(manuallyVerifiedCell);
                    }
                    sheet.autoSizeColumn(6);
                } else {
                    temporaryRow.createCell(6).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure());
                    temporaryRow.getCell(6).setCellStyle(genericCell);
                    sheet.autoSizeColumn(6);
                }

                rowPosition++;
            }
        }
        else {
            columnsTitles = new String[]{"Market","Feature","Test Run ID","Test Case title"};
            createHeader(sheet,columnsTitles);
            // Create the Cells and fill them with data, with added style
            for ( int indexTestRun = 0; indexTestRun < testrunList.size(); indexTestRun++){
                Row temporaryRow = sheet.createRow(rowPosition);
                temporaryRow.createCell(0).setCellValue(testrunList.get(indexTestRun).getMarket());
                temporaryRow.getCell(0).setCellStyle(genericCell);
                temporaryRow.createCell(1).setCellValue(testrunList.get(indexTestRun).getFeature());
                temporaryRow.getCell(1).setCellStyle(genericCell);
                temporaryRow.createCell(2).setCellFormula(
                        "HYPERLINK" +
                                "(\""+testrunList.get(indexTestRun).getUrlWebpage()+"\"," +
                                "\""+testrunList.get(indexTestRun).getTestRunName()+"\")");
                temporaryRow.getCell(2).setCellStyle(urlCell);
                temporaryRow.createCell(3).setCellValue(testrunList.get(indexTestRun).getTestCaseName());
                temporaryRow.getCell(3).setCellStyle(genericCell);
                rowPosition++;
            }
        }

        // AUTOSIZE all columns
        for ( int indexColumn = 0; indexColumn < 4; indexColumn++) {
            sheet.autoSizeColumn(indexColumn);
        }
        for ( int indexColumn = 6; indexColumn < 8; indexColumn++) {
            sheet.autoSizeColumn(indexColumn);
        }
        //Check and Update Title column if necessary
        if (sheet.getColumnWidth(3)>70) {sheet.setColumnWidth(3,70);}
    }

    /**
     * This method creates the Header (1st row) in the given sheet with given titles.
     * @param sheet sheet which will contain the header
     * @param columns column titles
     */
    private void createHeader (Sheet sheet, String[] columns){
        Font headerFont = workBookQualityReport.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        CellStyle headerCellStyle = workBookQualityReport.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setLocked(true);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        Row headerRow = sheet.getRow(0);
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.getCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        for ( int indexColumn = 0; indexColumn < columns.length; indexColumn++) {
            sheet.autoSizeColumn(indexColumn);
        }
    }

    /**
     * This method creates cells in a given range.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param rowAmount the amount rows which you would like to have
     * @param columnAmount the amount columns which you would like to have
     */
    private void createCells (XSSFSheet sheet, Integer rowAmount, Integer columnAmount){
        rowAmount=rowAmount+1; //userfriendly row number converted to id
        columnAmount=columnAmount+1; //userfriendly row number converted to id
        for (int rowIndex = 0; rowIndex < rowAmount; rowIndex++) {
            sheet.createRow(rowIndex);
            for (int columnIndex = 0; columnIndex < columnAmount; columnIndex++) {
                sheet.getRow(rowIndex).createCell(columnIndex);
            }
        }
    }

    /**
     * This method creates an xlsx file from workbook.
     */
    public void createFile(Workbook workbook, String scope) throws IOException {
        // PREPARE the random value for name
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm");
        String random = dateFormat.format(new Date());
        //CREATE file
        FileOutputStream fileOut = new FileOutputStream("autogeneratedReport_" + scope + "_" + random + ".xlsx");
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

    /**
     * This bunch of methods are filling theQualityIndex sheet with all the data for the graphs
     * @param sheet XSSFSheet where you would like to see the values.
     * @param testEnvironment is the test environment which you are working with.
     * @return an Integer which shows that how much rows can have data
     */
    private Integer setDataQualityIndexSheet (XSSFSheet sheet, environment testEnvironment) {
        //set Header
        String[] columnsTitles = new String[]{"PASSED","FAILED","OTHER","SUM","PASSED %","FAILED %","OTHER %",
                "QUALITY INDEX","QUALITY MINIMUM","not classified","application","Test script","content","access","data",
                "configuration"," App environment","TA environment","3rd party","passed manually","PASSED corrected","FAILED corrected",
                "PASSED corrected %","FAILED corrected %","auto-Classified","test execution time","avg time / test run (min)",
                "time (hours))","Not Classified corrected"};
        createHeader(sheet,columnsTitles);

        //check how much data can be maximum in a column
        Integer[] amountOfData = new Integer[]{testEnvironment.getPassed().size(),testEnvironment.getFailed().size(),testEnvironment.getOther().size()};
        Integer highestAmountOfData = 0;
        for (int indexDataAmount = 0; indexDataAmount < amountOfData.length; indexDataAmount++ ){
            if (amountOfData[indexDataAmount] > highestAmountOfData ) {
                highestAmountOfData = amountOfData [indexDataAmount];
            }
        }

        //-- passed, failed and other results starts with summary
        printValuesToColumn(sheet,testEnvironment.getPassed(),0);
        printValuesToColumn(sheet,testEnvironment.getFailed(),1);
        printValuesToColumn(sheet,testEnvironment.getOther(),2);
        printSumOfColumns(sheet,1,0,2,3,highestAmountOfData);
        //--passed, failed and other percentage starts
        printDivided(sheet,0,3,4,1,highestAmountOfData,true);
        printDivided(sheet,1,3,5,1,highestAmountOfData,true);
        printDivided(sheet,2,3,6,1,highestAmountOfData,true);
        //-- quality minimum starts
        for (int indexRows=1; indexRows <= highestAmountOfData; indexRows++){
            CellStyle percentageStyle = sheet.getWorkbook().createCellStyle();
            percentageStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(9)));
            sheet.getRow(indexRows).getCell(8).setCellStyle(percentageStyle);
            sheet.getRow(indexRows).getCell(8).setCellValue(0.7);
        }
        //-- classification starts
        printValuesToColumn(sheet,testEnvironment.getClassificationNotClassified(),9);
        printValuesToColumn(sheet,testEnvironment.getClassificationApplication(),10);
        printValuesToColumn(sheet,testEnvironment.getClassificationTestScript(),11);
        printValuesToColumn(sheet,testEnvironment.getClassificationContent(),12);
        printValuesToColumn(sheet,testEnvironment.getClassificationAccess(),13);
        printValuesToColumn(sheet,testEnvironment.getClassificationData(),14);
        printValuesToColumn(sheet,testEnvironment.getClassificationConfiguration(),15);
        printValuesToColumn(sheet,testEnvironment.getClassificationApplicationEnvironment(),16);
        printValuesToColumn(sheet,testEnvironment.getClassificationTestautomationEnvironment(),17);
        printValuesToColumn(sheet,testEnvironment.getClassificationThirdPartyScriptIssue(),18);
        printValuesToColumn(sheet,testEnvironment.getClassificationPassedManually(),19);
        //-- passed & failed correction starts based on passed manually
        printAdditionOrSubtraction(sheet,0,19,true,false,20,1,highestAmountOfData);
        printAdditionOrSubtraction(sheet,1,19,false,false,21,1,highestAmountOfData);
        //-- passed & failed corrected percentage
        printDivided(sheet,20,3,22,1,highestAmountOfData,true);
        printDivided(sheet,21,3,23,1,highestAmountOfData,true);
        //-- Quality Index
        printAdditionOrSubtraction(sheet,22,23,false,true,7,1,highestAmountOfData);
        //-- autoclassification starts
        printValuesToColumn(sheet,testEnvironment.getAutoClassifiedAmount(),24);
        //-- test execution time (minutes; hours), average time per test run (minutes)
        printValuesToColumn(sheet,testEnvironment.getRunTime(),25);
        printDivided(sheet,25,3,26,1,highestAmountOfData,false);
        printDividedByValue(sheet,25,60,27,1,highestAmountOfData,false);

        return highestAmountOfData;
    }

    /**
     * This method greates a graph to show the trends of seriesA, seriesB and seriesC together.
     * @param sheet  XSSFSheet where you would like to see the values.
     * @param title title of the graph
     * @param graphX0 cell id: corner point, horizontal starting point
     * @param graphY0 cell id: corner point, vertival starting poin
     * @param graphX1 cell id: corner point, horizontal ending point
     * @param firstRowIndex the first row in the sheet where the code will start to use the data
     * @param seriesAColumnIndex column id: where you can find data for seriesA
     * @param seriesBColumnIndex column id: where you can find data for seriesB
     * @param seriesCColumnIndex column id: where you can find data for seriesC
     * @param dataAmount shows how many rows should be checked
     */
    private void graphTestExecutionPercentage(XSSFSheet sheet, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer firstRowIndex, Integer seriesAColumnIndex, Integer seriesBColumnIndex, Integer seriesCColumnIndex, Integer dataAmount) {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        if ((graphX1-graphX0)<10){graphX1=graphX0+10;}
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,(graphY0+15)-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(numberingForGraph(dataAmount));
        XDDFNumericalDataSource ys1 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, seriesAColumnIndex, seriesAColumnIndex));
        XDDFNumericalDataSource ys2 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, seriesBColumnIndex, seriesBColumnIndex));
        XDDFNumericalDataSource ys3 =  XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, seriesCColumnIndex, seriesCColumnIndex));

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ys1);
        series1.setTitle("Other",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series2 = (XDDFLineChartData.Series) data.addSeries(xs,ys2);
        series2.setTitle("Failed",null);
        series2.setSmooth(true);
        series2.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series3 = (XDDFLineChartData.Series) data.addSeries(xs,ys3);
        series3.setTitle("Passed",null);
        series3.setSmooth(true);
        series3.setMarkerStyle(MarkerStyle.SQUARE);

        chart.setTitleText(title);
        chart.plot(data);
    }

    /**
     * This method creates the graph to show the trend of the quality index.
     * It uses 2 columns qualityIndex and qualityMinimum, which will be visible in the graph.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param title title of the graph
     * @param graphX0 cell id: corner point, horizontal starting point
     * @param graphY0 cell id: corner point, vertival starting poin
     * @param graphX1 cell id: corner point, horizontal ending point
     * @param firstRowIndex the first row in the sheet where the code will start to use the data
     * @param qualityIndexColumnIndex column id: where the quality index value is available
     * @param qualityMinimumColumnIndex column id: where the quality minimum value is available
     * @param dataAmount show how many rows should be checked
     */
    private void graphQualityIndexTrend(XSSFSheet sheet, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer firstRowIndex, Integer qualityIndexColumnIndex, Integer qualityMinimumColumnIndex, Integer dataAmount) {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        if ((graphX1-graphX0)<10){graphX1=graphX0+10;}
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,(graphY0+15)-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(numberingForGraph(dataAmount));

        XDDFNumericalDataSource ys1 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, qualityIndexColumnIndex, qualityIndexColumnIndex));
        XDDFNumericalDataSource ys2 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, qualityMinimumColumnIndex, qualityMinimumColumnIndex));

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ys1);
        series1.setTitle("Quality Index",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series2 = (XDDFLineChartData.Series) data.addSeries(xs,ys2);
        series2.setTitle("Quality Minimum",null);
        series2.setSmooth(false);
        series2.setMarkerStyle(MarkerStyle.NONE);

        chart.setTitleText(title);
        chart.plot(data);
    }

    /**
     * This method creates the graph to show the trends of the different defect classifications.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param testEnvironment the test environment which has the data model for the graph
     * @param title title of the graph
     * @param graphX0 cell id: corner point, horizontal starting point
     * @param graphY0 cell id: corner point, vertival starting poin
     * @param graphX1 cell id: corner point, horizontal ending point
     * @param firstRowIndex the first row in the sheet where the code will start to use the data
     * @param dataAmount shows how many rows should be checked
     * @param applicationColumnIndex column id: where Application Defect data available
     * @param scriptColumnIndex column id: where Script Issue data available
     * @param contentColumnIndex column id: where Content Issue data available
     * @param accessColumnIndex column id: where Access Issue data available
     * @param dataColumnIndex column id: where Data Issue data available
     * @param configColumnIndex column id: where Config Issue data available
     * @param environmentAppColumnIndex column id: where Environment Issue data available
     * @param notClassifiedColumnIndex column id: where Not Classified data available
     * @param passedManuallyColumnIndex column id: where during automated run failed, but manually passed data available
     */
    private void graphClassificationTrends(XSSFSheet sheet, environment testEnvironment, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer firstRowIndex, Integer dataAmount, Integer applicationColumnIndex, Integer scriptColumnIndex, Integer contentColumnIndex, Integer accessColumnIndex, Integer dataColumnIndex, Integer configColumnIndex, Integer environmentAppColumnIndex, Integer environmentTaColumnIndex, Integer thirdPartyColumnIndex, Integer notClassifiedColumnIndex, Integer passedManuallyColumnIndex){
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        if ((graphX1-graphX0)<10){graphX1=graphX0+10;}
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,(graphY0+25)-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(numberingForGraph(testEnvironment.getFailed().size()));
        XDDFNumericalDataSource ySeriesApplicationDefect = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, applicationColumnIndex, applicationColumnIndex));
        XDDFNumericalDataSource ySeriesScriptIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, scriptColumnIndex, scriptColumnIndex));
        XDDFNumericalDataSource ySeriesContentIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, contentColumnIndex, contentColumnIndex));
        XDDFNumericalDataSource ySeriesAccessIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, accessColumnIndex, accessColumnIndex));
        XDDFNumericalDataSource ySeriesDataIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, dataColumnIndex, dataColumnIndex));
        XDDFNumericalDataSource ySeriesConfigurationIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, configColumnIndex, configColumnIndex));
        XDDFNumericalDataSource ySeriesAppEnvironmentIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, environmentAppColumnIndex, environmentAppColumnIndex));
        XDDFNumericalDataSource ySeriesTaEnvironmentIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, environmentTaColumnIndex, environmentTaColumnIndex));
        XDDFNumericalDataSource ySeriesThirdPartyScriptIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, thirdPartyColumnIndex, thirdPartyColumnIndex));
        XDDFNumericalDataSource ySeriesNotClassifiedIssue = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, notClassifiedColumnIndex, notClassifiedColumnIndex));
        XDDFNumericalDataSource ySeriesPassedManually = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, passedManuallyColumnIndex, passedManuallyColumnIndex));

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesApplicationDefect);
        series1.setTitle("Application Defect",null);
        series1.setSmooth(false);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series2 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesScriptIssue);
        series2.setTitle("Script Issue",null);
        series2.setSmooth(false);
        series2.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series3 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesContentIssue);
        series3.setTitle("Content Issue",null);
        series3.setSmooth(false);
        series3.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series4 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesAccessIssue);
        series4.setTitle("Access Issue",null);
        series4.setSmooth(false);
        series4.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series5 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesDataIssue);
        series5.setTitle("Data Issue",null);
        series5.setSmooth(false);
        series5.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series6 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesConfigurationIssue);
        series6.setTitle("Configuration Issue",null);
        series6.setSmooth(false);
        series6.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series7 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesAppEnvironmentIssue);
        series7.setTitle("AvonShop Environment Issue",null);
        series7.setSmooth(false);
        series7.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series8 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesNotClassifiedIssue);
        series8.setTitle("Not Classified",null);
        series8.setSmooth(false);
        series8.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series9 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesPassedManually);
        series9.setTitle("Passed Manually",null);
        series9.setSmooth(false);
        series9.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series10 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesTaEnvironmentIssue);
        series10.setTitle("Automation Environment Issue",null);
        series10.setSmooth(false);
        series10.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series11 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesThirdPartyScriptIssue);
        series11.setTitle("3rd party Issue",null);
        series11.setSmooth(false);
        series11.setMarkerStyle(MarkerStyle.SQUARE);
        chart.setTitleText(title);
        chart.plot(data);
    }

    /**
     * This method creates a graph based on 1 single column to show the trends of that data.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param title title of the graph
     * @param graphX0 cell id: corner point, horizontal starting point
     * @param graphY0 cell id: corner point, vertival starting poin
     * @param graphX1 cell id: corner point, horizontal ending point
     * @param firstRowIndex the first row in the sheet where the code will start to use the data
     * @param dataColumnIndex column id: where the data available
     * @param dataAmount shows how many rows should be checked
     */
    private void graphSingleLine(XSSFSheet sheet, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer firstRowIndex, Integer dataColumnIndex, Integer dataAmount) {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        if ((graphX1-graphX0)<10){graphX1=graphX0+10;}
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,(graphY0+15)-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(numberingForGraph(dataAmount));

        XDDFNumericalDataSource ys1 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRowIndex, dataAmount, dataColumnIndex, dataColumnIndex));

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ys1);
        series1.setTitle("Testrun amount",null);
        series1.setSmooth(false);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        //somewhy we had multicolour line (each value in series got it's own colour)
        //therefore I tried to "force" 1 colour to the series.
        XDDFSolidFillProperties fillProperties = new XDDFSolidFillProperties(XDDFColor.from(hex2Rgb("#0077BE")));
        XDDFLineProperties lineProperties = new XDDFLineProperties();
        lineProperties.setFillProperties(fillProperties);
        XDDFShapeProperties shapeProperties = series1.getShapeProperties();
        if (shapeProperties == null) { shapeProperties = new XDDFShapeProperties();}
        shapeProperties.setLineProperties(lineProperties);
        series1.setShapeProperties(shapeProperties);

        chart.setTitleText(title);
        chart.plot(data);
    }

    /**
     * This method prints the values of an ArrayList into the given column of the given sheet.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param values The ArrayList of the values.
     * @param columnIndex The column where you would like to see the values.
     */
    private void printValuesToColumn(XSSFSheet sheet, ArrayList<Integer> values, Integer columnIndex){
        Integer rowIndex = values.size();
        for (int resultIndex = 0; resultIndex < values.size(); resultIndex++){
            sheet.getRow(rowIndex).getCell(columnIndex).setCellValue(values.get(resultIndex));
            rowIndex--;
        }
    }

    /**
     * This method summarizes the values in a given row and column range, and set the valuas to the given column
     * @param sheet XSSFSheet where you would like to see the values.
     * @param startingRowIndex the first row which the code should check ("input" and "output" values are also within same rows)
     * @param startColumnIndex the first column of the sum cell range
     * @param columnAmount amount of the columns in the cell range
     * @param sumColumn target columnt where you would like to see the sum values
     * @param dataAmount the amount of data, which refers to the amount of rows on the sheet
     */
    private void printSumOfColumns(XSSFSheet sheet, Integer startingRowIndex, Integer startColumnIndex, Integer columnAmount, Integer sumColumn, Integer dataAmount){
        Integer rowIndex = startingRowIndex;
        while (rowIndex <= dataAmount){
            double sum = 0.0;
            for (int columnIndex = startColumnIndex; columnIndex < columnAmount; columnIndex++){
                sum+=sheet.getRow(rowIndex).getCell(columnIndex).getNumericCellValue();
            }
            sheet.getRow(rowIndex).getCell(sumColumn).setCellValue((int)sum);
            rowIndex++;
        }
    }

    /**
     * This methods based the data from 2 given columns provide the result of their division.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param toBeDividedDataColumnIndex column with values which should be divided
     * @param dividerColumnIndex column with values which are the divider
     * @param resultColumnIndex column which will contain the result of the division
     * @param startingRowIndex the first row which the code should check ("input" and "output" values are also within same rows)
     * @param dataAmount the amount of data, which refers to the amount of rows on the sheet
     * @param isPercentage defines the style of the cells. If true, then "0 %" style, if not, then "0.00" style.
     */
    private void printDivided(XSSFSheet sheet, Integer toBeDividedDataColumnIndex, Integer dividerColumnIndex, Integer resultColumnIndex, Integer startingRowIndex, Integer dataAmount, Boolean isPercentage) {
        Integer rowIndex = startingRowIndex;
        CellStyle percentageStyle = sheet.getWorkbook().createCellStyle();
        percentageStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(9)));
        CellStyle averageStyle = sheet.getWorkbook().createCellStyle();
        averageStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(2)));

        while (rowIndex <= dataAmount){
            double dividedValue = sheet.getRow(rowIndex).getCell(toBeDividedDataColumnIndex).getNumericCellValue() / sheet.getRow(rowIndex).getCell(dividerColumnIndex).getNumericCellValue();
            if (isPercentage) {
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellStyle(percentageStyle);
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellValue(dividedValue);
            } else {
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellStyle(averageStyle);
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellValue(dividedValue);
            }
            rowIndex++;
        }
    }

    /**
     * This methods based the data from 1 given columns provide the result of the division of the column data and the divider.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param toBeDividedDataColumnIndex column with values which should be divided
     * @param divider value which is the divider
     * @param resultColumnIndex column which will contain the result of the division
     * @param startingRowIndex the first row which the code should check ("input" and "output" values are also within same rows)
     * @param dataAmount the amount of data, which refers to the amount of rows on the sheet
     * @param isPercentage defines the style of the cells. If true, then "0 %" style, if not, then "0.00" style.
     */
    private void printDividedByValue(XSSFSheet sheet, Integer toBeDividedDataColumnIndex, Integer divider, Integer resultColumnIndex, Integer startingRowIndex, Integer dataAmount, Boolean isPercentage) {
        Integer rowIndex = startingRowIndex;
        CellStyle percentageStyle = sheet.getWorkbook().createCellStyle();
        percentageStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(9)));
        CellStyle averageStyle = sheet.getWorkbook().createCellStyle();
        averageStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(2)));

        while (rowIndex <= dataAmount){
            double dividedValue = sheet.getRow(rowIndex).getCell(toBeDividedDataColumnIndex).getNumericCellValue() / divider;
            if (isPercentage) {
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellStyle(percentageStyle);
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellValue(dividedValue);
            } else {
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellStyle(averageStyle);
                sheet.getRow(rowIndex).getCell(resultColumnIndex).setCellValue(dividedValue);
            }
            rowIndex++;
        }
    }

    /**
     * This methods do an addition or subtraction based on given 2 columns and provide the result into the target column.
     * Result can have "0.00" or "0 %" style.
     * @param sheet XSSFSheet where you would like to see the values.
     * @param originalColumn left side of the operation
     * @param correctionColumn right side of the operation
     * @param isAddition Is it an addition? Yes: operation is +; No: operation is -.
     * @param targetColumn You can find the results here.
     * @param startingRowIndex the first row which the code should check ("input" and "output" values are also within same rows)
     * @param dataAmount the amount of data, which refers to the amount of rows on the sheet
     * @param isPercentage defines the style of the cells. If true, then "0 %" style, if not, then "0.00" style.
     */
    private void printAdditionOrSubtraction(XSSFSheet sheet, Integer originalColumn, Integer correctionColumn, Boolean isAddition, Boolean isPercentage, Integer targetColumn, Integer startingRowIndex, Integer dataAmount){
        Integer rowIndex = startingRowIndex;
        CellStyle percentageStyle = sheet.getWorkbook().createCellStyle();
        percentageStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(9)));
        CellStyle averageStyle = sheet.getWorkbook().createCellStyle();
        averageStyle.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(2)));
        while (rowIndex <= dataAmount){
            double newValue;
            if ( isAddition ) {
                newValue = sheet.getRow(rowIndex).getCell(originalColumn).getNumericCellValue() + sheet.getRow(rowIndex).getCell(correctionColumn).getNumericCellValue();
            } else {
                newValue = sheet.getRow(rowIndex).getCell(originalColumn).getNumericCellValue() - sheet.getRow(rowIndex).getCell(correctionColumn).getNumericCellValue();
            }
            sheet.getRow(rowIndex).getCell(targetColumn).setCellValue(newValue);
            if (isPercentage) {
                sheet.getRow(rowIndex).getCell(targetColumn).setCellStyle(percentageStyle);
            } else {
                sheet.getRow(rowIndex).getCell(targetColumn).setCellStyle(averageStyle);
            }
            rowIndex++;
        }
    }

    /**
     * This method is adding failure classification related summary to the sheet.
     * @param testEnvironment the test environment which has the data model for the graph
     * @param sheet the sheet which you would like to update
     * @param rowNumber the row where you would like to see the date
     * @param classification the classificationwhich you would like to see
     * @param style cell style for numeric values
     */
    private void setClassificationsOnExecutiveSummarySheet(environment testEnvironment, Sheet sheet, Integer rowNumber, String classification, CellStyle style){

        Integer currentClassificationAmount=0;
        Integer previousClassificationAmount=0;

        switch (classification) {
            case "Application Defect":
                currentClassificationAmount=testEnvironment.getClassificationApplication().get(0);
                previousClassificationAmount=testEnvironment.getClassificationApplication().get(1);
                break;
            case "Test Script issue":
                currentClassificationAmount=testEnvironment.getClassificationTestScript().get(0);
                previousClassificationAmount=testEnvironment.getClassificationTestScript().get(1);
                break;
            case "Content issue":
                currentClassificationAmount=testEnvironment.getClassificationContent().get(0);
                previousClassificationAmount=testEnvironment.getClassificationContent().get(1);
                break;
            case "Access issue":
                currentClassificationAmount=testEnvironment.getClassificationAccess().get(0);
                previousClassificationAmount=testEnvironment.getClassificationAccess().get(1);
                break;
            case "Data issue":
                currentClassificationAmount=testEnvironment.getClassificationData().get(0);
                previousClassificationAmount=testEnvironment.getClassificationData().get(1);
                break;
            case "Configuration issue":
                currentClassificationAmount=testEnvironment.getClassificationConfiguration().get(0);
                previousClassificationAmount=testEnvironment.getClassificationConfiguration().get(1);
                break;
            case "Avon Shop Environment issue":
                currentClassificationAmount=testEnvironment.getClassificationApplicationEnvironment().get(0);
                previousClassificationAmount=testEnvironment.getClassificationApplicationEnvironment().get(1);
                break;
            case "Test Automation Environment issue":
                currentClassificationAmount=testEnvironment.getClassificationTestautomationEnvironment().get(0);
                previousClassificationAmount=testEnvironment.getClassificationTestautomationEnvironment().get(1);
                break;
            case "3rd Party script issue":
                currentClassificationAmount=testEnvironment.getClassificationThirdPartyScriptIssue().get(0);
                previousClassificationAmount=testEnvironment.getClassificationThirdPartyScriptIssue().get(1);
                break;
            case "PASSED during manual check":
                currentClassificationAmount=testEnvironment.getClassificationPassedManually().get(0);
                previousClassificationAmount=testEnvironment.getClassificationPassedManually().get(1);
                break;
            default:
                break;
        }

        if  (classification.equalsIgnoreCase("PASSED during manual check")) {
            sheet.getRow(rowNumber).getCell(0).setCellValue(classification);
            sheet.getRow(rowNumber).getCell(1).setCellStyle(style);
            sheet.getRow(rowNumber).getCell(1).setCellValue(currentClassificationAmount);
        } else {
            sheet.getRow(rowNumber).getCell(0).setCellValue(classification);
            sheet.getRow(rowNumber).getCell(1).setCellStyle(style);
            sheet.getRow(rowNumber).getCell(1).setCellValue(currentClassificationAmount);
            if (currentClassificationAmount > previousClassificationAmount) {
                sheet.getRow(rowNumber).getCell(2).setCellValue("it has INCREASED by");
                sheet.getRow(rowNumber).getCell(3).setCellStyle(style);
                sheet.getRow(rowNumber).getCell(3).setCellValue(
                        currentClassificationAmount - previousClassificationAmount
                );
            }
            if (previousClassificationAmount > currentClassificationAmount) {
                sheet.getRow(rowNumber).getCell(2).setCellValue("it has DECREASED by");
                sheet.getRow(rowNumber).getCell(3).setCellStyle(style);
                sheet.getRow(rowNumber).getCell(3).setCellValue(
                        previousClassificationAmount - currentClassificationAmount
                );
            }
            if (currentClassificationAmount == previousClassificationAmount) {
                sheet.getRow(rowNumber).getCell(2).setCellValue("it has NOT CHANGED");

            }
            sheet.getRow(rowNumber).getCell(4).setCellValue(" comparing to the previous test execution");
        }
    }

    /**
     * This method generates the values for the horizontal axis.
     * Now this axis is showing the past executions in timely order.
     * @param amount The amount of the values which should be on the horizontal axis.
     * @return all the values for the horizontal axis
     */
    private String[] numberingForGraph (Integer amount) {
        String[] xAxis = new String[amount];
        Integer counter = 2;
        for (int indexArray = amount-1; indexArray >=0; indexArray--) {
            if (indexArray == (amount-1)) {
                xAxis[indexArray] = "latest run";
            } else {
                xAxis[indexArray] = serialization(Integer.toString(counter));
                counter++;
            }
        }
        return xAxis;
    }

    /**
     * A basic solution to have the numbers as an element of the series.
     * e.g. 1st, 2nd, 3rd, 13th, etc..
     * @param number the number which should be changed
     * @return the number with proper ending
     */
    private String serialization(String number) {
        switch (number.substring(number.length() - 1)) {
            case "1":
                number += "st";
                return number;
            case "2":
                number += "nd";
                return number;
            case "3":
                number += "rd";
                return number;
            default:
                number += "th";
                return number;
        }
    }

    /**
     * This method set the colour based on HEX code.
     * @param colorStr hexa code of the colour
     * @return the rgb code of the colour
     */
    private byte[] hex2Rgb(String colorStr) {
        int r = Integer.valueOf(colorStr.substring(1, 3), 16);
        int g = Integer.valueOf(colorStr.substring(3, 5), 16);
        int b = Integer.valueOf(colorStr.substring(5, 7), 16);
        return new byte[]{(byte) r, (byte) g, (byte) b};
    }

}
