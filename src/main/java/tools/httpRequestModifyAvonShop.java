package tools;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import pojo.environment;
import pojo.execution;
import pojo.testrun;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class httpRequestModifyAvonShop {
    String urlQtestAvonshop = "https://avon.qtestnet.com/api/v3/projects/";
    //Long projectId = baseIdentifiers.projectAvonShop.id;

    /**
     * This method send the PUT request to qTest to update the given test log.
     * @param testRunId qTest test run ID
     * @param testLogId qTest test log ID
     * @param startDate the start date&time of the given execution
     * @param endDate the end date&time of the given execution
     * @param classification classification of the failure
     * @param comment comment about the failure
     * @param verification verification status of the failed test execution
     * @return the responseEntity of the request
     */
    public HttpResponse updateLatestTestLog(Long projectId, Long testRunId, Long testLogId, String startDate, String endDate, String classification, String comment, String verification) throws Exception {

        String body = setBodyTestLogUpdate(testLogId, startDate, endDate, classification, comment, verification);
        StringEntity payload = new StringEntity(body, ContentType.APPLICATION_JSON);
        String bearer = "Bearer " + System.getProperty("qtestBearer");
        HttpPut request = setDefaultPUTrequest(bearer, projectId.toString() + "/test-runs/" + testRunId.toString() + "/test-logs/" + testLogId.toString());
        request.setEntity(new StringEntity(body, StandardCharsets.UTF_8));

        System.out.println(testRunId.toString() + " will be updated\n" +
                "URL is: https://avon.qtestnet.com/api/v3/projects/" + projectId.toString() + "/test-runs/" + testRunId.toString() + "/test-logs/" + testLogId.toString() +
                "\n");

        return getResponseEntityPUT(request);
    }

    /**
     * This method is setting up the default PUT request.
     * @param bearer authentication token
     * @param url last part of the url
     */
    private HttpPut setDefaultPUTrequest(String bearer, String url){

        HttpPut request = new HttpPut(urlQtestAvonshop +url);
        request.setHeader("authorization",bearer);
        request.setHeader("cache-control","no-cache");
        request.setHeader("content-type","application/json");
        request.setHeader("host","avon.qtestnet.com");
        request.setHeader("user-agent","AS-automation_reporter");
        request.setHeader("accept","*/*");
        request.setHeader("accept-encoding","gzip, deflate, br");

        return request;
    }

    /**
     * This method setting up the body of the request.
     * @param testLogId qTest test log ID
     * @param startDate the start date&time of the given execution
     * @param endDate the end date&time of the given execution
     * @param classification classification of the failure
     * @param comment comment of the failure
     * @param verification verification status of the failed test execution
     * @return the string of the body
     */
    private String setBodyTestLogUpdate (Long testLogId, String startDate, String endDate, String classification, String comment, String verification) {
        String body = "{\"id\": "+testLogId+",\n" +
                "\"exe_start_date\": \""+startDate+"\",\n" +
                "\"exe_end_date\": \""+endDate+"\",\n" +
                "\"properties\": [\n" +
                "    {\n" +
                "        \"field_id\": 9880650,\n" +
                "        \"field_value\": "+classification+"\n" +
                "    },\n" +
                "    {\n" +
                "        \"field_id\": 9880657,\n" +
                "        \"field_value\": "+verification+"\n" +
                "    },    \n" +
                "    {\n" +
                "        \"field_id\": 9751519,\n" +
                "        \"field_value\": \""+comment+"\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"field_id\": 10137126,\n" +
                "        \"field_value\": \"1\"\n" +//means it is auto-classified
                "    }\n" +
                "],\n" +
                "\"status\": {\n" +
                "    \"links\": [],\n" +
                "    \"id\": 602,\n" +
                "    \"name\": \"Failed\"\n" +
                "}\n" +
                "}";

        return body;
    }

    /**
     * This method is providing the response entity of the PUT request.
     * We are using it mainly to decide what HTTP code we got back.
     * @param givenRequest It should be an "HttpPut" entity already with url and headers.
     * @return the response entity for later usage
     */
    private HttpResponse getResponseEntityPUT(HttpPut givenRequest) throws Exception {
        if (givenRequest.getMethod().equalsIgnoreCase("PUT")) {
            CloseableHttpClient httpClientPut = HttpClientBuilder.create().build();
            HttpResponse response = httpClientPut.execute(givenRequest);

            return response;
        }
        throw new Exception("You need to give a PUT request (HttpPut) to this method. In any other cases it fails.");
    }

    /**
     * This method updates all failures within a given market.
     *
     * @param testEnvironment selected testEnvironment object
     * @param markets         list of the markets
     * @param classification  chosen classification
     * @param verification    chosen verification
     * @param comments        comment to the test log
     */
    public void updateAllFailuresInGivenMarkets(Long projectId, environment testEnvironment, ArrayList<String> markets, String classification, String verification, String comments, Boolean isOnlyClassificaion) throws Exception {
        httpRequestsGetAvonshop getter = new httpRequestsGetAvonshop();
        for (int indexGivenMarkets = 0; indexGivenMarkets < markets.size(); indexGivenMarkets++) {
            for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++) {
                if (testEnvironment.getMarkets()[indexMarket].getName().equalsIgnoreCase(markets.get(indexGivenMarkets))) {
                    System.out.println("Update has started for " + (markets.get(indexGivenMarkets) + " market." +
                            "\nTime is " + System.currentTimeMillis()) +
                            "##############################");
                    for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++) {
                        for (int indexTestRun = 0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++) {
                            testrun selectedTestRun = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun);
                            execution latestExecution = selectedTestRun.getExecutions().get(0);
                            if (latestExecution.getResult().equalsIgnoreCase("failed") && latestExecution.getClassificationOfFailure().isEmpty()) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                String startDate = dateFormat.format(latestExecution.getExecutionStartDate()) + "+00:00";
                                String endDate = dateFormat.format(latestExecution.getExecutionEndDate()) + "+00:00";
                                Long testRunId = selectedTestRun.getTestRunId();
                                Long testLogIdLatest = getter.getLatestTestLogId(projectId, selectedTestRun.getTestRunId());

                                HttpResponse response = updateLatestTestLog(
                                        projectId,
                                        testRunId,
                                        testLogIdLatest,
                                        startDate,
                                        endDate,
                                        classification,
                                        comments,
                                        verification);
                                if (!isOnlyClassificaion && response.getStatusLine().getStatusCode() == 200) {
                                    testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).setCommentOfFailure(comments);
                                    testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).setVerificationOfFailure(verification);
                                    testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).setClassificationOfFailure(classification);
                                    testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).setAutoClassified(true);
                                }
                            }
                        }
                    }
                    System.out.println("Update has finsihed for "+(markets.get(indexGivenMarkets)+" market." +
                            "\nTime is "+System.currentTimeMillis())+
                            "##############################");
                }
            }
        }
    }
}
