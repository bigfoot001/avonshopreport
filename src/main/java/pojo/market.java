package pojo;

import java.util.ArrayList;

public class market {
    String name; //There is only Getter, to make sure that it won't change unintentionally.
    Long testCycleId; //There is only Getter, to make sure that it won't change unintentionally.
    feature[] features;
    Integer testRunAmount;
    public ArrayList<Integer> passed;
    public ArrayList<Integer> failed;
    public ArrayList<Integer> other;
    public ArrayList<Integer> autoClassifiedAmount;

    /**
     * This method construct a market object with the name and the test cycle id.
     * @param name name of the market
     * @param testCycleId id of the TestCycle which contains all the testruns for the given market
     */
    public market(String name, Long testCycleId) {
        this.name = name;
        this.testCycleId = testCycleId;
        this.testRunAmount=0;
        this.passed = new ArrayList<Integer>();
        this.failed = new ArrayList<Integer>();
        this.other = new ArrayList<Integer>();
        this.autoClassifiedAmount = new ArrayList<Integer>();
    }

    public String getName() {
        return name;
    }

    public Long getTestCycleId() {
        return testCycleId;
    }

    public feature[] getFeatures() {
        return features;
    }

    public void setFeatures(feature[] features) {
        this.features = features;
    }

    public Integer getTestRunAmount() {
        return testRunAmount;
    }

    public void setTestRunAmount(Integer testRunAmount) {
        this.testRunAmount = testRunAmount;
    }

    public ArrayList<Integer> getPassed() {
        return passed;
    }

    public ArrayList<Integer> getFailed() {
        return failed;
    }

    public ArrayList<Integer> getOther() {
        return other;
    }

    public ArrayList<Integer> getAutoClassifiedAmount() {
        return autoClassifiedAmount;
    }

    public void setAutoClassifiedAmount(ArrayList<Integer> autoClassifiedAmount) {
        this.autoClassifiedAmount = autoClassifiedAmount;
    }
}
