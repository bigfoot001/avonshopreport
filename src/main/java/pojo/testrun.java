package pojo;

import java.util.ArrayList;

public class testrun {
    ArrayList<execution> executions;
    String lastStatus;
    String Env;
    String Market;
    String testRunName;
    Long testRunId; //There is only Getter, to make sure that it won't change unintentionally.
    Long testRunParentId;
    String testRunParentType;
    String testCaseName;
    String testCaseVersion;
    Long testCaseId;
    Long testCaseVersionId;
    String feature;
    String urlApi;
    String urlWebpage;
    Integer similarFailureId;



    /**
     * Creates an "empty" test run which should be filled up with data.
     * @param testRunId id of the test run
     */
    public testrun (Long testRunId){
        this.testRunId = testRunId;
    }

    public Long getTestRunId() {
        return testRunId;
    }
    public ArrayList<execution> getExecutions() {
    return executions;
}

    public void setExecutions(ArrayList<execution> executions) {
        this.executions = executions;
    }

    public String getLastStatus() {
        return lastStatus;
    }

    public void setLastStatus(String lastStatus) {
        this.lastStatus = lastStatus;
    }

    public String getEnv() {
        return Env;
    }

    public void setEnv(String env) {
        Env = env;
    }

    public String getMarket() {
        return Market;
    }

    public void setMarket(String market) {
        Market = market;
    }

    public String getTestRunName() {
        return testRunName;
    }

    public void setTestRunName(String testRunName) {
        this.testRunName = testRunName;
    }

    public Long getTestRunParentId() {
        return testRunParentId;
    }

    public void setTestRunParentId(Long testRunParentId) {
        this.testRunParentId = testRunParentId;
    }

    public String getTestRunParentType() {
        return testRunParentType;
    }

    public void setTestRunParentType(String testRunParentType) {
        this.testRunParentType = testRunParentType;
    }

    public String getTestCaseName() {
        return testCaseName;
    }

    public void setTestCaseName(String testCaseName) {
        this.testCaseName = testCaseName;
    }

    public String getTestCaseVersion() {
        return testCaseVersion;
    }

    public void setTestCaseVersion(String testCaseVersion) {
        this.testCaseVersion = testCaseVersion;
    }

    public Long getTestCaseId() {
        return testCaseId;
    }

    public void setTestCaseId(Long testCaseId) {
        this.testCaseId = testCaseId;
    }

    public Long getTestCaseVersionId() {
        return testCaseVersionId;
    }

    public void setTestCaseVersionId(Long testCaseVersionId) {
        this.testCaseVersionId = testCaseVersionId;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public String getUrlApi() {
        return urlApi;
    }

    public void setUrlApi(String urlApi) {
        this.urlApi = urlApi;
    }

    public String getUrlWebpage() {
        return urlWebpage;
    }

    public void setUrlWebpage(String urlWebpage) {
        this.urlWebpage = urlWebpage;
    }

    public Integer getSimilarFailureId() {
        return similarFailureId;
    }

    public void setSimilarExecutionFailureId(Integer similarFailureId) {
        this.similarFailureId = similarFailureId;
    }

}
