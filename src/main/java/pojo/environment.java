package pojo;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class environment {
    String name; //There is only Getter, to make sure that it won't change unintentionally.
    Long projectId; //There is only Getter, to make sure that it won't change unintentionally.
    Long testCycleId; //There is only Getter, to make sure that it won't change unintentionally.
    public ArrayList<Integer> passed;
    public ArrayList<Integer> failed;
    public ArrayList<Integer> other;
    public ArrayList<Integer> classificationApplication;
    public ArrayList<Integer> classificationTestScript;
    public ArrayList<Integer> classificationContent;
    public ArrayList<Integer> classificationAccess;
    public ArrayList<Integer> classificationData;
    public ArrayList<Integer> classificationConfiguration;
    public ArrayList<Integer> classificationApplicationEnvironment;
    public ArrayList<Integer> classificationTestautomationEnvironment;
    public ArrayList<Integer> classificationThirdPartyScriptIssue;
    public ArrayList<Integer> classificationNotClassified;
    public ArrayList<Integer> classificationPassedManually;
    public ArrayList<Integer> autoClassifiedAmount;
    public ArrayList<Integer> runTime;
    public ArrayList<LocalDateTime> builIdlist;
    market[] markets;
    Integer testRunAmountLatest;

    /**
     * This method construct an environment object with the name, project id and the test cycle id.
     * @param name name of the environment
     * @param testCycleId id of the TestCycle which contains all the test runs for the given environment
     */
    public environment(String name,Long testCycleId, Long projectId){
        this.name = name;
        this.testCycleId = testCycleId;
        this.projectId=projectId;
        this.testRunAmountLatest = 0;
        this.passed = new ArrayList<>();
        this.failed = new ArrayList<>();
        this.other = new ArrayList<>();
        this.classificationApplication = new ArrayList<>();
        this.classificationTestScript = new ArrayList<>();
        this.classificationContent = new ArrayList<>();
        this.classificationAccess = new ArrayList<>();
        this.classificationData = new ArrayList<>();
        this.classificationConfiguration = new ArrayList<>();
        this.classificationApplicationEnvironment = new ArrayList<>();
        this.classificationTestautomationEnvironment = new ArrayList<>();
        this.classificationThirdPartyScriptIssue = new ArrayList<>();
        this.classificationNotClassified = new ArrayList<>();
        this.classificationPassedManually = new ArrayList<>();
        this.autoClassifiedAmount = new ArrayList<>();
        this.runTime = new ArrayList<>();
    }
    public String getName() {
        return name;
    }

    public market[] getMarkets() {
        return markets;
    }

    public void setMarkets(market[] markets) {
        this.markets = markets;
    }

    public Long getTestCycleId() {
        return testCycleId;
    }

    public Integer getTestRunAmountLatest() {
        return testRunAmountLatest;
    }

    public void setTestRunAmountLatest(Integer testRunAmountLatest) {
        this.testRunAmountLatest = testRunAmountLatest;
    }

    public Long getProjectId() {
        return projectId;
    }

    public ArrayList<Integer> getPassed() {
        return passed;
    }

    public ArrayList<Integer>getFailed() {
        return failed;
    }

    public ArrayList<Integer> getOther() {
        return other;
    }

    public ArrayList<Integer> getClassificationApplication() {
        return classificationApplication;
    }

    public void setClassificationApplication(ArrayList<Integer> classificationApplication) {
        this.classificationApplication = classificationApplication;
    }

    public ArrayList<Integer> getClassificationTestScript() {
        return classificationTestScript;
    }

    public void setClassificationTestScript(ArrayList<Integer> classificationTestScript) {
        this.classificationTestScript = classificationTestScript;
    }

    public ArrayList<Integer> getClassificationContent() {
        return classificationContent;
    }

    public void setClassificationContent(ArrayList<Integer> classificationContent) {
        this.classificationContent = classificationContent;
    }

    public ArrayList<Integer> getClassificationAccess() {
        return classificationAccess;
    }

    public void setClassificationAccess(ArrayList<Integer> classificationAccess) {
        this.classificationAccess = classificationAccess;
    }

    public ArrayList<Integer> getClassificationData() {
        return classificationData;
    }

    public void setClassificationData(ArrayList<Integer> classificationData) {
        this.classificationData = classificationData;
    }

    public ArrayList<Integer> getClassificationConfiguration() {
        return classificationConfiguration;
    }

    public void setClassificationConfiguration(ArrayList<Integer> classificationConfiguration) {
        this.classificationConfiguration = classificationConfiguration;
    }

    public ArrayList<Integer> getClassificationApplicationEnvironment() {
        return classificationApplicationEnvironment;
    }

    public void setClassificationApplicationEnvironment(ArrayList<Integer> classificationApplicationEnvironment) {
        this.classificationApplicationEnvironment = classificationApplicationEnvironment;
    }

    public ArrayList<Integer> getClassificationNotClassified() {
        return classificationNotClassified;
    }

    public void setClassificationNotClassified(ArrayList<Integer> classificationNotClassified) {
        this.classificationNotClassified = classificationNotClassified;
    }

    public ArrayList<Integer> getClassificationPassedManually() {
        return classificationPassedManually;
    }

    public void setClassificationPassedManually(ArrayList<Integer> classificationPassedManually) {
        this.classificationPassedManually = classificationPassedManually;
    }

    public ArrayList<Integer> getAutoClassifiedAmount() {
        return autoClassifiedAmount;
    }

    public ArrayList<Integer> getRunTime() {
        return runTime;
    }

    public ArrayList<Integer> getClassificationTestautomationEnvironment() {
        return classificationTestautomationEnvironment;
    }

    public void setClassificationTestautomationEnvironment(ArrayList<Integer> classificationTestautomationEnvironment) {
        this.classificationTestautomationEnvironment = classificationTestautomationEnvironment;
    }

    public ArrayList<Integer> getClassificationThirdPartyScriptIssue() {
        return classificationThirdPartyScriptIssue;
    }

    public void setClassificationThirdPartyScriptIssue(ArrayList<Integer> classificationThirdPartyScriptIssue) {
        this.classificationThirdPartyScriptIssue = classificationThirdPartyScriptIssue;
    }
}
