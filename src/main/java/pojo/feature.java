package pojo;

import java.util.ArrayList;

public class feature {
    public String name; //There is only Getter, to make sure that it won't change unintentionally.
    public Long testCycleId; //There is only Getter, to make sure that it won't change unintentionally.
    public ArrayList<Integer> passed;
    public ArrayList<Integer> failed;
    public ArrayList<Integer> other;
    public ArrayList<testrun> testruns;
    public Integer testRunAmount;
    public ArrayList<Integer> autoClassifiedAmount;

    /**
     * This method construct an feature object with the name and the test cycle id.
     * @param name name of the feature
     * @param testCycleId id of the TestCycle which contains all the testruns for the given feature
     */
    public feature(String name, Long testCycleId) {
        this.name = name;
        this.testCycleId = testCycleId;
        this.passed = new ArrayList<>();
        this.failed = new ArrayList<>();
        this.other = new ArrayList<>();
        this.testruns = new ArrayList<>();
        this.autoClassifiedAmount = new ArrayList<>();
        this.testRunAmount=0;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Integer> getPassed() {
        return passed;
    }

    public ArrayList<Integer> getFailed() {
        return failed;
    }

    public ArrayList<Integer> getOther() {
        return other;
    }

    public Long getTestCycleId() {
        return testCycleId;
    }

    public ArrayList<testrun> getTestruns() {
        return testruns;
    }

    public Integer getTestRunAmount() {
        return testRunAmount;
    }

    public void setTestRunAmount() {
        this.testRunAmount = this.testruns.size();
    }

    public ArrayList<Integer> getAutoClassifiedAmount() {
        return autoClassifiedAmount;
    }

    public void setAutoClassifiedAmount(ArrayList<Integer> autoClassifiedAmount) {
        this.autoClassifiedAmount = autoClassifiedAmount;
    }

}
