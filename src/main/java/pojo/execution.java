package pojo;

import java.util.Date;

public class execution {
    String result;
    Date executionEndDate;
    Date executionStartDate;
    String causeOfFailure;
    String classificationOfFailure;
    String verificationOfFailure;
    String commentOfFailure;
    String stepOfFailure;
    Boolean isAutoClassified;
    String buildIdentifier;

    /**
     * This method contructs the execution object with result and base dates.
     * @param result (passed or failed) result of the execution
     * @param executionEndDate when the execution finished
     * @param executionStartDate when the execution has started
     */
    public execution(String result, Date executionEndDate, Date executionStartDate){
        this.result = result;
        this.executionEndDate = executionEndDate;
        this.executionStartDate = executionStartDate;
        this.causeOfFailure = "";
        this.classificationOfFailure = "";
        this.verificationOfFailure = "";
        this.commentOfFailure = "";
        this.stepOfFailure = "";
        this.buildIdentifier= "";
        this.isAutoClassified = false;
    }

    public String getResult() {
        return result;
    }

    public Date getExecutionEndDate() {
        return executionEndDate;
    }

    public Date getExecutionStartDate() {
        return executionStartDate;
    }

    public String getCauseOfFailure() {
        return causeOfFailure;
    }

    public void setCauseOfFailure(String causeOfFailure) {
        this.causeOfFailure = causeOfFailure;
    }

    public String getClassificationOfFailure() {
        return classificationOfFailure;
    }

    public void setClassificationOfFailure(String classificationOfFailure) {
        this.classificationOfFailure = classificationOfFailure;
    }

    public String getVerificationOfFailure() {
        return verificationOfFailure;
    }

    public void setVerificationOfFailure(String verificationOfFailure) {
        this.verificationOfFailure = verificationOfFailure;
    }

    public String getCommentOfFailure() {
        return commentOfFailure;
    }

    public void setCommentOfFailure(String commentOfFailure) {
        this.commentOfFailure = commentOfFailure;
    }

    public String getStepOfFailure() {
        return stepOfFailure;
    }

    public void setStepOfFailure(String stepOfFailure) {
        this.stepOfFailure = stepOfFailure;
    }

    public Boolean getAutoClassified() {
        return isAutoClassified;
    }

    public void setAutoClassified(Boolean autoClassified) {
        isAutoClassified = autoClassified;
    }

    public String getBuildIdentifier() {
        return buildIdentifier;
    }

    public void setBuildIdentifier(String buildIdentifier) {
        this.buildIdentifier = buildIdentifier;
    }
}
