package enums;

public enum baseIdentifiers {
    dbEnvQafAll(3932652L, "QAF_DB"),
    dbEnvQamAll(3932652L, "QAM_DB"),
    projectDigitalBrochure(109424L, "AvonShop"),
    asEnvQafAll(3704990L, "QAF_ALL"),
    asEnvQamTop(3804077L, "QAM_TOP8"),
    asEnvQamAll(3663602L, "QAM_ALL"),
    asEnvProd(3666111L, "PROD_SMOKE"),
    projectAvonShop(92680L, "AvonShop");

    public final Long id;
    public final String name;

    baseIdentifiers(Long id, String name) {
        this.id = id;
        this.name = name;
    }
}
