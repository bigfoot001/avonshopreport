package enums;

public enum failureClassification {
    applicationDefect("1"),
    testScriptIssue("2"),
    contentIssue("4"),
    accessIssue("5"),
    dataIssue("6"),
    configurationIssue("7"),
    environmentApplicationIssue("8"),
    environmentAutomationIssue("9"),
    thirdPartyScriptIssue("10"),
    empty("\"\"");

    public final String label;

    failureClassification(String label) {
        this.label = label;
    }
}
