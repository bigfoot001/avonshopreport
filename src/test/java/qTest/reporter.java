package qTest;

import org.apache.poi.ss.usermodel.Workbook;
import org.json.simple.JSONArray;
import org.junit.Test;
import pojo.environment;
import pojo.testrun;
import tools.buildExcelWorkbook;
import tools.dataBuilder;

import static enums.baseIdentifiers.*;

public class reporter {

    /**
     * This test case creates a workbook report with graphs and summary for the failure analyses.
     */
    @Test
    public void createReportAsQamTop() throws Exception {
        //runs about 10 minutes
        Long environmentId = asEnvQamTop.id;
        Long projectId = projectAvonShop.id;
        String environmentName = asEnvQamTop.name;

        createReport(projectId, environmentId, environmentName);
    }

    @Test
    public void createReportAsQamAll() throws Exception {
        //runs about 25 minutes
        Long environmentId = asEnvQamAll.id;
        String environmentName = asEnvQamAll.name;
        Long projectId = projectAvonShop.id;

        createReport(projectId, environmentId, environmentName);
    }

    @Test
    public void createReportAsQafAll() throws Exception {
        //runs about 5 minutes, focused on DD markets, other descoped
        Long environmentId = asEnvQafAll.id;
        String environmentName = asEnvQafAll.name;
        Long projectId = projectAvonShop.id;

        createReport(projectId, environmentId, environmentName);
    }

    @Test
    public void createReportProdSmoke() throws Exception {
        //runs about 10 minutes
        Long environmentId = asEnvProd.id;
        String environmentName = asEnvProd.name;
        Long projectId = projectAvonShop.id;

        createReport(projectId, environmentId, environmentName);
    }

    @Test
    public void createReportDbQafAll() throws Exception {

        Long environmentId = dbEnvQafAll.id;
        String environmentName = dbEnvQafAll.name;
        Long projectId = projectDigitalBrochure.id;

        createReport(projectId, environmentId, environmentName);
    }

    private void createReport(Long projectId, Long environmentId, String environmentName) throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        String bearer = "Bearer " + System.getProperty("qtestBearer");

        JSONArray rawListTestRuns = objectCreator.getAllTestRunsFromParent(projectId, bearer, environmentId.toString(), "test-cycle");

        testrun[] testruns = objectCreator.createTestRuns(
                projectId,
                rawListTestRuns,
                bearer,
                10,
                false,
                environmentId.toString()
        );

        environment testEnvironment = objectCreator.createTestEnvironment(projectId, environmentId);
        objectCreator.buildUpTestEnvironment(testruns, testEnvironment);

        buildExcelWorkbook workbookCreator = new buildExcelWorkbook();
        Workbook workbookQualityReport = workbookCreator.createWorkbookQualityReport(testEnvironment);

        workbookCreator.createFile(workbookQualityReport, environmentName);
    }



}

