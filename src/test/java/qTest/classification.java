package qTest;

import enums.baseIdentifiers;
import enums.failureClassification;
import enums.failureManualVerification;
import org.json.simple.JSONArray;
import org.junit.Test;
import pojo.environment;
import pojo.testrun;
import tools.dataBuilder;
import tools.httpRequestModifyAvonShop;

import java.util.ArrayList;
import java.util.Arrays;

import static enums.baseIdentifiers.*;


public class classification {

    /**
     * This test case in case of failures updates the latest test logs based on previous/historical data.
     */
    @Test
    public void autoClassifyAsQamAll() throws Exception {
        dataBuilder objectCreator = new dataBuilder();

        Long environmentId = asEnvQamAll.id;
        Long projectId = projectAvonShop.id;
        //Get Data and create testEnvironment model
        environment testEnvironment = getData(projectId, environmentId, 30, objectCreator);

        ArrayList<testrun> testRunsProposedForCopy = createListForUpdate(testEnvironment, objectCreator);
        //Send the PUT requests to update the testruns
        objectCreator.updateTestLog(baseIdentifiers.projectAvonShop.id, testRunsProposedForCopy, testEnvironment, true);

    }

    @Test
    public void autoClassifyAsQamTop() throws Exception {
        dataBuilder objectCreator = new dataBuilder();

        Long environmentId = asEnvQamTop.id;
        Long projectId = projectAvonShop.id;
        //Get Data and create testEnvironment model
        environment testEnvironment = getData(projectId, environmentId, 30, objectCreator);

        ArrayList<testrun> testRunsProposedForCopy = createListForUpdate(testEnvironment, objectCreator);
        //Send the PUT requests to update the testruns
        objectCreator.updateTestLog(baseIdentifiers.projectAvonShop.id, testRunsProposedForCopy, testEnvironment, true);

    }

    @Test
    public void autoClassifyAsQafAll() throws Exception {
        dataBuilder objectCreator = new dataBuilder();

        Long environmentId = asEnvQafAll.id;
        Long projectId = projectAvonShop.id;
        //Get Data and create testEnvironment model
        environment testEnvironment = getData(projectId, environmentId, 30, objectCreator);

        ArrayList<testrun> testRunsProposedForCopy = createListForUpdate(testEnvironment, objectCreator);
        //Send the PUT requests to update the testruns
        objectCreator.updateTestLog(projectId, testRunsProposedForCopy, testEnvironment, true);

    }

    @Test
    public void autoClassifyDbQafAll() throws Exception {
        dataBuilder objectCreator = new dataBuilder();

        Long environmentId = dbEnvQafAll.id;
        Long projectId = projectDigitalBrochure.id;
        //Get Data and create testEnvironment model
        environment testEnvironment = getData(projectId, environmentId, 30, objectCreator);

        ArrayList<testrun> testRunsProposedForCopy = createListForUpdate(testEnvironment, objectCreator);
        //Send the PUT requests to update the testruns
        objectCreator.updateTestLog(projectId, testRunsProposedForCopy, testEnvironment, true);

    }

    @Test
    public void autoClassifyDbQamAll() throws Exception {
        dataBuilder objectCreator = new dataBuilder();

        Long environmentId = dbEnvQamAll.id;
        Long projectId = projectDigitalBrochure.id;
        //Get Data and create testEnvironment model
        environment testEnvironment = getData(projectId, environmentId, 30, objectCreator);

        ArrayList<testrun> testRunsProposedForCopy = createListForUpdate(testEnvironment, objectCreator);
        //Send the PUT requests to update the testruns
        objectCreator.updateTestLog(projectId, testRunsProposedForCopy, testEnvironment, true);

    }

    /**
     * This test case in case of failures updates the latest test logs based GIVEN data in the GIVEN markets.
     * It can be used to send "batch request" for changes. e.g. if all tests in a market failed due to the same reason.
     */
    @Test
    public void failureBatchClassificationInGivenMarkets() throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        Long projectId = projectAvonShop.id;
        Long environmentId = asEnvQamTop.id;
        environment testEnvironment = getData(projectId, environmentId, 1, objectCreator);

        //PREPARE the update
        httpRequestModifyAvonShop poster = new httpRequestModifyAvonShop();

        //DEFINE markets
        ArrayList<String> markets = new ArrayList<>(
                Arrays.asList("TR")
        );

        //DEFINE comment, qTest.classification and verification
        String comments = "test message again during script implementation\\n modified by Automated Script";
        String classification = failureClassification.configurationIssue.label;
        String verification = failureManualVerification.failManually.label;

        //SEND the update
        poster.updateAllFailuresInGivenMarkets(
                projectId, testEnvironment, markets, classification, verification, comments, true
        );
    }

    private environment getData(Long projectId, Long environmentId, Integer amountOfLastRuns, dataBuilder objectCreator) throws Exception {
        String bearer = "Bearer " + System.getProperty("qtestBearer");

        JSONArray rawListTestRuns = objectCreator.getAllTestRunsFromParent(projectId, bearer, environmentId.toString(), "test-cycle");

        testrun[] testruns = objectCreator.createTestRuns(
                projectId,
                rawListTestRuns,
                bearer,
                amountOfLastRuns,
                true,
                environmentId.toString()
        );

        environment testEnvironment = objectCreator.createTestEnvironment(projectId, environmentId);
        objectCreator.buildUpTestEnvironment(testruns, testEnvironment);
        return testEnvironment;
    }

    private ArrayList<testrun> createListForUpdate(environment testEnvironment, dataBuilder objectCreator) {
        ArrayList<ArrayList<testrun>> testRunsGrouped = objectCreator.getLastTwoResultsForComparison(testEnvironment);
        /*
        0 - passAgain
        1 - failAgain
        2 - failNew
        3 - passNew
        4 - other
         */
        ArrayList<testrun> testRunsFailedAgain = testRunsGrouped.get(1);
        ArrayList<testrun> testRunsProposedForCopy = new ArrayList<>();
        //DO THE COMPARISON BETWEEN Failures - FAILED AGAIN group (last 2 failed)
        objectCreator.updateChangeList(testRunsFailedAgain, testRunsProposedForCopy);

        ArrayList<testrun> testRunsNewFailed = testRunsGrouped.get(2);
        //DO THE COMPARISON BETWEEN Failures - NEW FAILED group (last failed, previous not failed)
        objectCreator.updateChangeList(testRunsNewFailed, testRunsProposedForCopy);

        return testRunsProposedForCopy;
    }
}
