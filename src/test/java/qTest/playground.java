package qTest;

import enums.baseIdentifiers;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.simple.JSONArray;
import org.junit.Test;
import pojo.environment;
import pojo.testrun;
import tools.buildExcelWorkbook;
import tools.dataBuilder;

import java.util.ArrayList;

import static enums.baseIdentifiers.asEnvQamTop;
import static enums.baseIdentifiers.projectAvonShop;

public class playground {
    /**
     * This test case in case of failures updates the latest test logs based on previous/historical data,
     * and after that creates a workbook report with graphs and summary for the failure analyses.
     */
    @Test
    public void updateCurrentFailuresANDcreateWorkBook() throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        Long projectId = projectAvonShop.id;
        Long environmentId = asEnvQamTop.id;
        String bearer = "Bearer " + System.getProperty("qtestBearer");

        JSONArray rawListTestRuns = objectCreator.getAllTestRunsFromParent(projectId, bearer, environmentId.toString(), "test-cycle");

        testrun[] testruns = objectCreator.createTestRuns(
                projectId,
                rawListTestRuns,
                bearer,
                30,
                false,
                environmentId.toString()
        );

        environment testEnvironment = objectCreator.createTestEnvironment(projectId, environmentId);
        objectCreator.buildUpTestEnvironment(testruns, testEnvironment);

        ArrayList<ArrayList<testrun>> testRunsGrouped = objectCreator.getLastTwoResultsForComparison(testEnvironment);
        /*
        0 - passAgain
        1 - failAgain
        2 - failNew
        3 - passNew
        4 - other
         */
        ArrayList<testrun> testRunsFailedAgain = testRunsGrouped.get(1);
        ArrayList<testrun> testRunsProposedForCopy = new ArrayList<>();
        ArrayList<testrun> testRunsNewFailed = testRunsGrouped.get(2);
        //DO THE COMPARISON BETWEEN Failures - FAILED AGAIN group (last 2 failed)
        objectCreator.updateChangeList(testRunsFailedAgain, testRunsProposedForCopy);
        //DO THE COMPARISON BETWEEN Failures - NEW FAILED group (last failed, previous not failed)
        objectCreator.updateChangeList(testRunsNewFailed, testRunsProposedForCopy);

        //Send the PUT requests to update the testruns
        objectCreator.updateTestLog(baseIdentifiers.projectAvonShop.id, testRunsProposedForCopy, testEnvironment, false);

        //TODO: UPDATE all elements in "testRunsProposedForCopy" after post we should use a get

        //CREATE WORKBOOK

        //new httpRequestsGet().closeConnectionGet();
        //new httpRequestModify().closeConnectionPut();

        buildExcelWorkbook workbookCreator = new buildExcelWorkbook();
        Workbook workbookQualityReport = workbookCreator.createWorkbookQualityReport(testEnvironment);
        workbookCreator.createFile(workbookQualityReport, baseIdentifiers.asEnvQamTop.name);


    }
}
