package qTest;

import enums.baseIdentifiers;
import enums.failureClassification;
import enums.failureManualVerification;
import org.apache.http.HttpResponse;
import org.json.simple.JSONArray;
import org.junit.Test;
import org.qas.qtest.api.auth.PropertiesQTestCredentials;
import org.qas.qtest.api.auth.QTestCredentials;
import org.qas.qtest.api.services.design.TestDesignService;
import org.qas.qtest.api.services.design.TestDesignServiceClient;
import org.qas.qtest.api.services.design.model.GetTestCaseRequest;
import org.qas.qtest.api.services.design.model.TestCase;
import pojo.environment;
import pojo.testrun;
import tools.dataBuilder;
import tools.httpRequestModifyAvonShop;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import static enums.baseIdentifiers.asEnvQamTop;
import static enums.baseIdentifiers.projectAvonShop;

public class units {

    /**
     * This test case reads the details of a test case with the standard qTest library methods.
     * Official qTest methods are barely used in the project, because they are not covering all the needs.
     */
    @Test
    public void read1TestCase() throws IOException {
        Long testCaseId = 39156855L;
        Long testCaseVersion = 57125305L;

        QTestCredentials credentials = new PropertiesQTestCredentials(new ByteArrayInputStream(("token = Bearer " + System.getProperties().get("qtestBearer")).getBytes()));

        TestDesignService testDesignService = new TestDesignServiceClient(credentials);
        testDesignService.setEndpoint("avon.qtestnet.com");

        GetTestCaseRequest getTestCaseRequest = new GetTestCaseRequest().withProjectId(baseIdentifiers.projectAvonShop.id).withTestCaseId(testCaseId).withTestCaseVersion(testCaseVersion);
        TestCase testCase = testDesignService.getTestCase(getTestCaseRequest);

    }

    /**
     * This test case collects all information from qTest and creates the data model.
     * If you would like to try/check some actions on the data, you just need to run this test case till the "BREAK POINT".
     */
    @Test
    public void createStructureWithDetails() throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        String bearer = "Bearer " + System.getProperties().get("bearer").toString();
        Long projectId = projectAvonShop.id;
        Long environmentId = asEnvQamTop.id;

        JSONArray rawListTestRuns = objectCreator.getAllTestRunsFromParent(projectId, bearer, environmentId.toString(), "test-cycle");

        testrun[] testruns = objectCreator.createTestRuns(
                projectId,
                rawListTestRuns,
                bearer,
                30,
                false,
                environmentId.toString()
        );

        environment testEnvironment = objectCreator.createTestEnvironment(projectId, environmentId);
        objectCreator.buildUpTestEnvironment(testruns, testEnvironment);

        System.out.println("BREAK POINT!");
    }

    @Test
    public void update1LastTestLog() throws Exception {
        httpRequestModifyAvonShop setter = new httpRequestModifyAvonShop();
        String startDate = "2022-02-21T11:10:26+00:00";
        String endDate = "2022-02-21T11:10:26+00:00";
        //next 3 variable can and should be updated
        String classification = failureClassification.applicationDefect.label;
        String comment = "test update by script - second";
        String verification = failureManualVerification.failManually.label;
        //get data before test - coming soon..., till that can be checked via postman call


        //update
        HttpResponse responsePut = setter.updateLatestTestLog(baseIdentifiers.projectAvonShop.id, 143177383L, 226199821L, startDate, endDate, classification, comment, verification);
        System.out.println(responsePut.getEntity().getContent().toString());

        //get data after test - coming soon..., till that can be checked via postman call


    }
}
